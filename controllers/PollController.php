<?php

namespace app\controllers;

use Yii;
use app\api\modules\v1\models\Polls;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\api\modules\v1\models\PollsQuestionAnswer;

/**
 * PollController implements the CRUD actions for Polls model.
 */
class PollController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Polls models.
     * @return mixed
     */
    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => Polls::find()->where(['active' => 1]),
        ]);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Polls model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Polls model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        
        $model = new Polls();

        if ($model->load(Yii::$app->request->post())) {
            
            if($model->save()){

            $model->answer = $_POST['Polls']['answer'];
            $sess = $model->session;
            $eventpoll = $model->event_id;

            foreach ($model->answer as $oneObj) {
                $ans = new PollsQuestionAnswer();
                $ans->pollsanswerid = $oneObj;
                // var_dump($oneObj);

                $model->link('pollsquestionAnswers', $ans);
                //$model->link('pollsquestionAnswers', $sess);

                Yii::$app->db->createCommand()->update('pollsquestion_answer', ['sessionid' => $sess,'events_id' => $eventpoll], ['pollsquestionid' => $model->id])->execute();
                //Yii::$app->db->createCommand("INSERT INTO pollsquestion_answer(`sessionid`) VALUES ('$sess')")->execute();
            }

            return $this->redirect(['view', 'id' => $model->id]);
            
        }
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Polls model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            PollsQuestionAnswer::deleteAll('pollsquestionid = :id', [':id' => $id]);

            $model->answer = $_POST['Polls']['answer'];
            $sess = $model->session;

            foreach ($model->answer as $oneObj) {
                $ans = new PollsQuestionAnswer();
                $ans->pollsanswerid = $oneObj;
                $model->link('pollsquestionAnswers', $ans);

                Yii::$app->db->createCommand()->update('pollsquestion_answer', ['sessionid' => $sess], ['pollsquestionid' => $model->id])->execute();
            }

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Polls model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        Yii::$app->db->createCommand()->update('polls', ['active' => 0], ['id' => $id,])->execute();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Polls model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Polls the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionSuccess() {
          $this->layout = 'loginLayout';
        return $this->render('success');
    }

    public function actionQuestion() {
        $this->layout = 'loginLayout';
        if (isset($_GET['session']) && isset($_GET['userid'])) {
            $id = $_GET['session'];
            $userid = $_GET['userid'];
            $model = Polls::find()->where(['session' => $id])->andWhere(['active' => '1'])->one();
            if (count($model) > 0) {
                return $this->render('question', [
                            'model' => $model,
                            'userid' => $userid,
                ]);
            } else {
                return $this->render('error', [
                            'error' => 'No Polling Created Yet.',
                ]);
            }
        } else {
            return $this->render('error', [
                        'error' => 'Session Id and User Id are both required.',
            ]);
        }
    }
    
    public function actionPollsanalytics() {
        return $this->render('pollsanalytics');
    }
    
    public function actionPollsanalytics2() {
        return $this->render('pollsanalytics2');
    }

    public function actionQuestionanswered() {
        $this->layout = 'loginLayout';
        $qtn = $_POST['id'];
        $user = $_POST['userid'];
        $ans = $_POST['Polls']['userans'];

        $model = $this->findModel($qtn);
        $sessid = $model->session;
        $usercount = Polls::getcountuserans($user,$model->id);
        
        if ($usercount =='0'){

            Yii::$app->db->createCommand("INSERT INTO pollsuser_answers(`pollsid`,`pollstypeid`,`userid`,`sessionid`,`numanswer`) VALUES ('$qtn','$ans','$user', '$sessid', '1')")->execute();
        }
        elseif ($usercount =='1'){
            Yii::$app->db->createCommand("INSERT INTO pollsuser_answers(`pollsid`,`pollstypeid`,`userid`,`sessionid`,`numanswer`) VALUES ('$qtn','$ans','$user', '$sessid', '2')")->execute();
        
        }
        else{
            return $this->render('error', [
                        'error' => 'You have exceeded your limit of answering the question. '
                . 'Thank you for participating !!',
                    ]);
        }
        Yii::$app->session->setFlash('success', "Successfully answered question");

        return $this->redirect(['success']);

    }

    protected function findModel($id) {
        if (($model = Polls::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    public function actionGetcount() {
        
        $qtnid = $_POST['qtnid'];
        
        $model = $this->findModel($qtnid);
        
        return $this->redirect(array('pollsanalytics2', 
                        'model' => $model,
                        'qtnid' => $qtnid)
            );
        
        
    }

}