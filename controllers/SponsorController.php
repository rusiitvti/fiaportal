<?php

namespace app\controllers;

use Yii;
use app\api\modules\v1\models\Sponsors;
use app\api\modules\v1\models\SearchSponsors;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\UploadedFile;

/**
 * SponsorController implements the CRUD actions for Sponsors model.
 */
class SponsorController extends Controller {

    public function init() {
        $userid = Yii::$app->session['user_id'];
        if ($userid == '') {
            $this->redirect(Yii::$app->params['server'] . Yii::$app->params['servername'] . '/site/login');
        }
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Sponsors models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new SearchSponsors();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['active' => 1]);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Sponsors model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Sponsors model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Sponsors();

        if ($model->load(Yii::$app->request->post())) {
            $servername = Yii::$app->params['servername'];
            if ($model->file !== '') {
                $model->file = UploadedFile::getInstance($model, 'logo');
                $model->file->saveAs('uploads/sponsors/' . $model->file->baseName . '.' . $model->file->extension);
                $model->logo = "" . $servername . '/uploads/sponsors/' . $model->file->baseName . '.' . $model->file->extension . "";
            }
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                
            }
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Sponsors model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $servername = Yii::$app->params['servername'];
            $templogo = UploadedFile::getInstance($model, 'logo');
            if ($templogo !== null) {
                $model->file = UploadedFile::getInstance($model, 'logo');
                $model->file->saveAs('uploads/sponsors/' . $model->file->baseName . '.' . $model->file->extension);
                $model->logo = "" . $servername . '/uploads/sponsors/' . $model->file->baseName . '.' . $model->file->extension . "";
            }
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Sponsors model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {

        Yii::$app->db->createCommand()->update('sponsors', ['active' => 0], ['id' => $id,])->execute();
        //$this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Sponsors model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Sponsors the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Sponsors::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
