<?php

namespace app\controllers;

use Yii;
use app\api\modules\v1\models\Events;
use app\api\modules\v1\models\SearchEvents;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use app\api\modules\v1\models\EventsAtendees;
use app\api\modules\v1\models\EventsSponsors;
use yii\filters\AccessControl;
use yii\web\Session;
use yii\helpers\Json;
use app\api\modules\v1\models\User;
use \Gumlet\ImageResize;

/**
 * EventController implements the CRUD actions for Events model.
 */
class ProfileController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Events models.
     * @return mixed
     */
    public function actionIndex() {
        
        echo 'This is profile index';

        $searchModel = new SearchEvents();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['active' => 1]);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Events model.
     * @param integer $id
     * @param integer $venue_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        $this->layout = 'loginLayout';
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Events model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
//        $model = new Events();
//        //get the instance of the uploaded file
//
//        if ($model->load(Yii::$app->request->post())) {
//            $servername = Yii::$app->params['servername'];
//            //to-do check if the file exist
//            //if it is overwrite>??
//
//            if ($model->file !== '') {
//                $model->file = UploadedFile::getInstance($model, 'file');
//                $model->file->saveAs('uploads/events/' . $model->file->baseName . '.' . $model->file->extension);
//                $model->image_cover = "" . $servername . '/uploads/events/' . $model->file->baseName . '.' . $model->file->extension . "";
//            }
//            if ($model->save()) {
//                return $this->redirect(['view', 'id' => $model->id, 'venue_id' => $model->venue_id]);
//            }
//        }
//        return $this->render('create', [
//                    'model' => $model,
//        ]);
    }

    /**
     * Updates an existing Events model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @param integer $venue_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate() {
        if (isset($_GET['user_id'])) {
            $this->layout = 'loginLayout';
            $userid = $_GET['user_id'];
            $model = $this->findModel($userid);
            if ($model->load(Yii::$app->request->post())) {
                $servername = Yii::$app->params['servername'];
                $originalPassword = $model->getOldAttribute('password');
                $username = $model->getOldAttribute('username');
                $oldphone = $model->getOldAttribute('phone');

                if ($model->confirmnewpass !== '') {
                    if ($model->confirmnewpass == $model->newpassword) {
                        $hashPassword = Yii::$app->getSecurity()->generatePasswordHash($model->confirmnewpass);
                        $model->password = $hashPassword;
                    }
                }
                $tempfile = UploadedFile::getInstance($model, 'file');

                if ($tempfile !== null) {
                    $servername = Yii::$app->params['servername'];
                    $imagename = $this->random_string();
                    //upload the file
                    $model->file = UploadedFile::getInstance($model, 'file');
                    //var_dump($model->file->tempName);
                    //   exit;
                    $path = 'uploads/user/' . $imagename . '.' . $model->file->extension;
                    $newimage = new ImageResize($model->file->tempName);
//                    $newimage->resizeToHeight(100);
//                    $newimage->resizeToWidth(100);
                    $newimage->scale(50);
                    // $newimage->resize(500, 300, $allow_enlarge = True);
                    $newimage->save($path);

                    // $model->file->saveAs('uploads/user/' . $imagename . '.' . $model->file->extension);
                    //get the url for saving it in mysql
                    $model->picture = "" . $servername . '/uploads/user/' . $imagename . '.' . $model->file->extension . "";
                }

                $model->save(false);
                return $this->redirect(['view', 'id' => $model->id]);
            }
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Events model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @param integer $venue_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
//    public function actionDelete($id, $venue_id) {
//
//        Yii::$app->db->createCommand()->update('events', ['active' => 0], ['id' => $id, 'venue_id' => $venue_id,])->execute();
//        //$this->findModel($id, $venue_id)->delete();
//
//        return $this->redirect(['index']);
//    }

    /**
     * Finds the Events model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @param integer $venue_id
     * @return Events the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = User::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function random_string() {
        $length = 8;
        $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));

        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }

        return $key;
    }

    /*
     * Assign users to an event
     * add users to events attentdees
     */
//
//    public function actionAssign() {
//
//        return $this->render('assign');
//    }
//
//    public function actionAssignevent() {
//        $eventid = $_POST['eventid'];
//        $userid = $_POST['users'];
////        var_dump($userid);
////        exit;
//        //assume all validatuion have been completed
//        $finalUserArr = [];
//        foreach ($userid as $anId) {
//            $temp = [$eventid, $anId];
//            array_push($finalUserArr, $temp);
//        }
//        $sucess = EventsAtendees::assignUsers($finalUserArr);
//        echo $sucess;
//    }
//
//    /*
//     * remove user from an event
//     */
//
//    public function actionDeactivateuser() {
//        $eventid = $_POST['eventid'];
//        $userid = $_POST['users'];
//
//        foreach ($userid as $anId) {
//            //$temp = [$userid, $anId];
//            $result = EventsAtendees::deactivateAttendees($eventid, $anId);
//        }
//        //  $sucess = EventsSponsors::assignSponsors($finalSponsorArr);
//        echo $result;
//    }
//
//    /*
//     * Assign speakers to an event
//     */
//
//    public function actionAssignsponsor() {
//        return $this->render('assignSponsors');
//    }
//
//    public function actionTransfersponsor() {
//        $eventid = $_POST['eventid'];
//        $sponsorid = $_POST['sponsors'];
//
//        //assume all validatuion have been completed
//        $finalSponsorArr = [];
//        foreach ($sponsorid as $anId) {
//            $temp = [$eventid, $anId];
//            array_push($finalSponsorArr, $temp);
//        }
//        $sucess = EventsSponsors::assignSponsors($finalSponsorArr);
//        echo $sucess;
//    }
//
//    public function actionGetnotusers() {
//        $eventid = $_POST['eventid'];
//        $eventattendees = EventsAtendees::getusersnot($eventid);
//
//        $data = $this->renderPartial('_attendpartial', array('eventattendees' => $eventattendees), false, true);
//        return Json::encode($data);
//    }
}
