<?php

namespace app\controllers;

use Yii;
use app\api\modules\v1\models\Events;
use app\api\modules\v1\models\SearchEvents;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use app\api\modules\v1\models\EventsAtendees;
use app\api\modules\v1\models\EventsSponsors;
use yii\filters\AccessControl;
use yii\web\Session;
use yii\helpers\Json;

/**
 * EventController implements the CRUD actions for Events model.
 */
class EventController extends Controller {

    public function init() {
        $userid = Yii::$app->session['user_id'];
        if ($userid == '') {
            $this->redirect(Yii::$app->params['server'] . Yii::$app->params['servername'] . '/site/login');
        }
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
//            'access' => [
//                'class' => AccessControl::className(),
//                'only' => ['create', 'update', 'view'],
//                'rules' => [
//                    // allow authenticated users
//                    [
//                        'allow' => true,
//                        'roles' => ['@'],
//                    ],
//                // everything else is denied
//                ],
//            ],
        ];
    }

    /**
     * Lists all Events models.
     * @return mixed
     */
    public function actionIndex() {

        $searchModel = new SearchEvents();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['active' => 1]);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Events model.
     * @param integer $id
     * @param integer $venue_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id, $venue_id) {
        return $this->render('view', [
                    'model' => $this->findModel($id, $venue_id),
        ]);
    }
    
    public function actionEventattendees() {
        
        if (!empty($_POST)){
            
        $eventid = $_POST['event_id'];
        
        return $this->redirect(array('eventattendees', 
                    'eventid' => $eventid)
        );
        }
        return $this->render('eventattendees');
    
    }

    public function actionEventsponsors() {
        
        if (!empty($_POST)){
            
        $eventid = $_POST['event_id'];
        
        return $this->redirect(array('eventsponors', 
                    'eventid' => $eventid)
        );
        }
        return $this->render('eventattendees');
    
    }
    
    public function actionInactiveuser(){
        
        $id= $_GET['id'];
        $eventid = $_GET['eventid'];
        Yii::$app->db->createCommand("DELETE FROM eventattendees WHERE events_id = '$eventid' and user_id = '$id' ")->execute();
        //$this->findModel($id)->delete();

        return $this->redirect(['eventattendees']);
        
    }

    /**
     * Creates a new Events model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Events();
        //get the instance of the uploaded file

        if ($model->load(Yii::$app->request->post())) {
            $servername = Yii::$app->params['servername'];
            //to-do check if the file exist
            //if it is overwrite>??

            if ($model->file !== '') {
                $model->file = UploadedFile::getInstance($model, 'file');
                $model->file->saveAs('uploads/events/' . $model->file->baseName . '.' . $model->file->extension);
                $model->image_cover = "" . $servername . '/uploads/events/' . $model->file->baseName . '.' . $model->file->extension . "";
            }
            if ($model->save(false)) {
                return $this->redirect(['view', 'id' => $model->id, 'venue_id' => $model->venue_id]);
            }
        }
        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Events model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @param integer $venue_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id, $venue_id) {
        $model = $this->findModel($id, $venue_id);

        if ($model->load(Yii::$app->request->post())) {
            $servername = Yii::$app->params['servername'];
            //to-do check if the file exist
            //if it is overwrite>??
             $templogo = UploadedFile::getInstance($model, 'file');
//            var_dump($templogo);
//            exit;
             if ($templogo !== null) {
                $model->file = UploadedFile::getInstance($model, 'file');
                $model->file->saveAs('uploads/events/' . $model->file->baseName . '.' . $model->file->extension);
                $model->image_cover = "" . $servername . '/uploads/events/' . $model->file->baseName . '.' . $model->file->extension . "";
            }
            if ($model->save(false)) {
                return $this->redirect(['view', 'id' => $model->id, 'venue_id' => $model->venue_id]);
            }
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Events model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @param integer $venue_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id, $venue_id) {

        Yii::$app->db->createCommand()->update('events', ['active' => 0], ['id' => $id, 'venue_id' => $venue_id,])->execute();
        //$this->findModel($id, $venue_id)->delete();

        return $this->redirect(['index']);
    }


    public function actionAssign() {

         if (!empty($_POST)){
            
        $eventid = $_POST['event_id'];
        
        return $this->redirect(array('assign', 
                    'eventid' => $eventid)
        );
        }
        return $this->render('assign');

        //return $this->render('assign');
    }

    /**
     * Finds the Events model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @param integer $venue_id
     * @retur
     n Events the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $venue_id) {
        if (($model = Events::findOne(['id' => $id, 'venue_id' => $venue_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }


//     public function actionAssignevent() {
//         $eventid = $_POST['eventid'];
//         $userid = $_POST['users'];
// //        var_dump($userid);
// //        exit;
//         //assume all validatuion have been completed
//         //$finalUserArr = [];
//         foreach ($userid as $anId) {
//             // $temp = [$eventid, $anId];
//             // array_push($finalUserArr, $temp);

//             Yii::$app->db->createCommand()->batchInsert('eventattendees', ['events_id'=>$eventid], ['user_id'=>$anId,])->execute();
//         }
//         return $this->redirect(['assign']);
//     }

    /*
     * Assign users to an event
     * add users to events attentdees
     */

    

    public function actionAssignevent() {
        $eventid = $_POST['eventid'];
        $userid = $_POST['users'];
//        var_dump($userid);
//        exit;
        //assume all validatuion have been completed
        $finalUserArr = [];
        foreach ($userid as $anId) {
            $temp = [$eventid, $anId];
            array_push($finalUserArr, $temp);
        }
        $sucess = EventsAtendees::assignUsers($finalUserArr);
        echo $sucess;
    }

    /*
     * remove user from an event
     */

    public function actionDeactivateuser() {
        $eventid = $_POST['eventid'];
        $userid = $_POST['users'];

        foreach ($userid as $anId) {
            //$temp = [$userid, $anId];
            $result = EventsAtendees::deactivateAttendees($eventid, $anId);
        }
        //  $sucess = EventsSponsors::assignSponsors($finalSponsorArr);
        echo $result;
    }

    /*
     * Assign speakers to an event
     */

    public function actionAssignsponsor() {
        return $this->render('assignSponsors');
    }

    public function actionTransfersponsor() {
        $eventid = $_POST['eventid'];
        $sponsorid = $_POST['sponsors'];

        //assume all validatuion have been completed
        $finalSponsorArr = [];
        foreach ($sponsorid as $anId) {
            $temp = [$eventid, $anId];
            array_push($finalSponsorArr, $temp);
        }
        $sucess = EventsSponsors::assignSponsors($finalSponsorArr);
        echo $sucess;
    }

    public function actionGetnotusers() {
        $eventid = $_POST['eventid'];//this is the event id passed from the ajax post
        $eventattendees = EventsAtendees::getusersnot($eventid);//get the list of users not have been assigned

        $data = $this->renderPartial('_attendpartial', array('eventattendees' => $eventattendees), false, true);//this is renderpartial the view
        return Json::encode($data);//remember to json encode the data.....__attendpartial.php
    }

    public function actionGetnotsponsors() {
        $eventid = $_POST['eventid'];//this is the event id passed from the ajax post
        $eventsponors = EventsSponsors::getsponsorsnot($eventid);//get the list of sponsors not have been assigned

        $data = $this->renderPartial('_attendsponsors', array('eventsponors' => $eventsponors), false, true);//this is renderpartial the view
        return Json::encode($data);//remember to json encode the data.....__attendsponsors.php
    }

}
