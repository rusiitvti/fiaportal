<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\web\Session;
use app\api\modules\v1\models\EventsSponsors;
use app\api\modules\v1\models\UserHasType;


//use Yii;
use app\api\modules\v1\models\Events;
use app\api\modules\v1\models\SearchEvents;
use app\api\modules\v1\models\SearchUser;
//use yii\web\Controller;
use yii\web\NotFoundHttpException;
//use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use app\api\modules\v1\models\EventsAtendees;
use app\api\modules\v1\models\SpeakersReview;
//app\api\modules\v1\models\SpeakersReview
//use app\api\modules\v1\models\EventsSponsors;
//use yii\filters\AccessControl;
//use yii\web\Session;
use yii\helpers\Json;

class SiteController extends Controller {

    public function behaviors() {
        
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex() {

        if (!Yii::$app->user->isGuest) {
            return $this->render('index');
        } else {
            //  $this->layout = 'loginLayout';
<<<<<<< HEAD
            return $this->redirect('site/login');//if infinite loop add array('site/login')
=======
            return $this->redirect(array('site/login'));
>>>>>>> c192da466b27642619e1207f0604215ebe4f79e8
        }
    }

    public function actionDashboard() {

        //if (!Yii::$app->user->isGuest) {
        return $this->render('index');
//        } else {
//            //  $this->layout = 'loginLayout';
//            return $this->redirect('site/login');
//        }
    }

    public function actionLogin() {
        $this->layout = 'loginLayout';
        if (!Yii::$app->user->isGuest) {
            Yii::$app->user->login();
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $session = Yii::$app->session;

            $session['user_id'] = Yii::$app->user->identity->id;
            $userid = $session['user_id'];
            $success = UserHasType::getUsertypes($userid);
            if ($success == 'allow') {
                $this->layout = 'main';
                return $this->render('index');
            } else {
                $this->layout = 'loginLayout';
                return $this->render('disallowed');
            }
        }
        return $this->render('login', [
                    'model' => $model,
        ]);
    }

    public function actionLogout() {


//        var_dump($session);
//        exit;
        return $this->redirect('site/login');
    }

    public function actionContact() {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
                    'model' => $model,
        ]);
    }

    public function actionAbout() {
        return $this->render('about');
    }

    public function actionDestroylogin() {
        Yii::$app->user->logout();
        Yii::$app->session->remove('user_id');
        return $this->redirect('site/logout');
    }


    //  public function actionGetnotsponsors() {
    //     return $this->redirect('site/login');
    // }

     public function actionGetnotsponsors() {
        $eventsponors = $_POST['eventid'];//this is the event id passed from the ajax post
        //$eventsponors = EventsAtendees::getsessionusers($eventid);//get the list of sponsors not have been assigned

        $data = $this->renderPartial('_index', array('eventsponors' => $eventsponors), false, true);//this is renderpartial the view
        return Json::encode($data);//remember to json encode the data.....__attendsponsors.php
    }

}
