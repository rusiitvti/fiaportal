<?php

namespace app\controllers;

use Yii;
use app\api\modules\v1\models\Notification;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\api\modules\v1\models\EventsNotifications;

/**
 * NotificationController implements the CRUD actions for Notification model.
 */
class NotificationController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Notification models.
     * @return mixed
     */
    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => Notification::find()->where(['active'=>1]),
        ]);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Notification model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Notification model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Notification();

        if ($model->load(Yii::$app->request->post())) {
            $title = $model->title;
            $body = $model->body;
            //build data for ios
            $iosdata = array('to' => '/topics/allios',
                'notification' => array('title' => $title, 'text' => $body,'sound' => 'default'),
                'priority' => 'high'
            );
            
            $androidData = array('to' => '/topics/allandroid', 'data' => array('name' => $title, 'body' => $body));
            
            //sent the push notification
            $this->sendNotification($iosdata);
            $this->sendNotification($androidData);
            
            $model->sent = 1;
            $model->save();

            $eventnotice = new EventsNotifications();
            
            $eventnotice->events_id = $model->eventid;
            $eventnotice->notification_id = $model->id;
            $eventnotice->save();
            
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    protected function sendNotification($data) {
        $headers = array
            (
            'Authorization: key=AAAASmzT4PQ:APA91bFTH-Ykxy-BOopVnHqYlG7Th5mqVsFlUSJ_cdmHYLP8ZFruUNKBQ1xtzrMLH_QFW60R9fsKHFSSsK3dNeFbAp949WlazUOQlcvNCAlPeSkVUOfMxDckaTYsR87Gq6LxZOFllTfU',
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        $result = curl_exec($ch);
        //option - do we check for curl exception
        //or check for FCM return results
        if (curl_errno($ch)) {
            //there was an error in sending message
            //log error - to-do
        }
        curl_close($ch);

        echo $result;
    }

    /**
     * Updates an existing Notification model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Notification model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        //$this->findModel($id)->delete();

        
        Yii::$app->db->createCommand()->update('notification', ['active' => 0], ['id' => $id])->execute();
        //$this->findModel($id, $venue_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Notification model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Notification the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Notification::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
