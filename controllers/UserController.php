<?php

namespace app\controllers;

use Yii;
use app\api\modules\v1\models\User;
use app\api\modules\v1\models\SearchUser;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\UploadedFile;
use \app\api\modules\v1\models\UserHasType;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function init() {
        $userid = Yii::$app->session['user_id'];
        if ($userid == '') {
            $this->redirect(Yii::$app->params['server'] . Yii::$app->params['servername'] . '/site/login');
        }
        parent::init();
    }
    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SearchUser();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
     public function actionCreate() {

        $servername = Yii::$app->params['servername'];
        $model = new User();
        if ($model->load(Yii::$app->request->post())) {

            $transaction = Yii::$app->db->beginTransaction();
            try {
                //trim the username because its going to be unique for filename
                $imagename = preg_replace('/\s+/', '', $model->username);
                //hash the password
               
                $model->username = $model->email;
                
                $model->password = $this->random_string();
                $pw = $model->password;
                $hashPassword = Yii::$app->getSecurity()->generatePasswordHash($model->password);
                $model->password = $hashPassword;
                $tempfile = UploadedFile::getInstance($model, 'file');
                if ($tempfile !== null) {

                    // $imagename = preg_replace('/\s+/', '', $tempfile);
                    //upload the file
                    $model->file = UploadedFile::getInstance($model, 'file');
                    $model->file->saveAs('uploads/user/' . $tempfile);
                    //get the url for saving it in mysql
                    $model->picture = "" . $servername . '/uploads/user/' . $tempfile . "";
                } else {
                    $model->picture = "" . $servername . '/uploads/user/default.png' . "";
                }
                //get the user type

                if ($model->save()) {
                    $model->usertype = $_POST['User']['usertype'];
                    foreach ($model->usertype as $oneObj) {
                        $utype = new UserHasType();
                        $utype->userType_id = $oneObj;
                        $model->link('userhastypes', $utype);
                    }
                    $transaction->commit();
                } else {
                    // show errors
                    $usenrmaeerror = $model->getErrors('username');
                    foreach ($usenrmaeerror as $key => $value) {
                        $error = $value;
                    }
                    return $this->redirect(['error', 'errors' => $error]);
                    // exit;
                }
                $useremail = $model->email;
                $this->sendUserInfos($useremail, $pw);
                return $this->redirect(['view', 'id' => $model->id]);
            } catch (Exception $e) {

                $transaction->rollBack();
            }
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        
        $servername = Yii::$app->params['servername'];
        $model = $this->findModel($id);
//       Yii::$app->params['originalUsertype'] = $model->usertype;
        if ($model->load(Yii::$app->request->post())) {

            $model->username = $model->email;
            $tempfile = UploadedFile::getInstance($model, 'file');
            if ($tempfile !== null) {
                
                // $imagename = preg_replace('/\s+/', '', $tempfile);
                //upload the file
                $model->file = UploadedFile::getInstance($model, 'file');
                $model->file->saveAs('uploads/user/' . $tempfile);
                //get the url for saving it in mysql
                $model->picture = "" . $servername . '/uploads/user/' . $tempfile . "";
            } 
//            else {
//                $model->picture = "" . $servername . '/uploads/user/default.png' . "";
//            }
//            $newusertypes = $_POST['User']['usertype'];
//             var_dump($newusertypes);
//             exit;
            
            if ($model->save()) {
                
                UserHasType::deleteAll('user_id = :id', [':id' => $id]);
                
                    $model->usertype = $_POST['User']['usertype'];
                    foreach ($model->usertype as $oneObj) {
                        $utype = new UserHasType();
                        $utype->userType_id = $oneObj;
                        $model->link('userhastypes', $utype);
                    }
//                    $transaction->commit();
                } else {
                    // show errors
                    $usenrmaeerror = $model->getErrors('username');
                    foreach ($usenrmaeerror as $key => $value) {
                        $error = $value;
                    }
                    return $this->redirect(['error', 'errors' => $error]);
                    // exit;
                }
            
            
            $model->save(false);


            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {

        Yii::$app->db->createCommand()->update('user', ['active' => 0], ['id' => $id,])->execute();
        //$this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionError($errors) {
        return $this->render('error', [
                    'error' => $errors,
        ]);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionActivateusers() {
        return $this->render('activateUsers');
    }
    
    public function actionResetpassword() {
        return $this->render('resetpassword');
    }
    
    public function actionResetpw() {
        
        $email = $_POST['User']['email'];
        
        //echo $email;
        
        $newpw = $this->random_string();
        
        //echo $newpw;
        
        $hashnewpw = Yii::$app->getSecurity()->generatePasswordHash($newpw);
        
        Yii::$app->db->createCommand()->update('user', ['password' => $hashnewpw ], ['username' => $email,])->execute();
        
        //echo $hashnewpw;
        $this->sendResetPw($email, $newpw);
        
        Yii::$app->session->setFlash('success', "Password has been reset.");

        return $this->redirect(['resetpassword']);
        
        
        
    }

    public function actionActivate() {
        //$eventid = $_POST['eventid'];
        $userid = $_POST['users'];

        //assume all validatuion have been completed
        //$finalUserArr = [];
        foreach ($userid as $anId) {
            Yii::$app->db->createCommand()->update('user', ['active' => 1], ['id' => $anId,])->execute();
        }

        return $this->redirect(['activateusers']);
//        $sucess = EventsAtendees::assignUsers($finalUserArr);
//        echo $sucess;
    }
    
        protected function sendResetPw($useremail, $password) {
        $htmlContent = '<html>
                        <head><title>Welcome to FIA APP</title></head>
                        <body>
                            <h1>Password Reset</h1>
                            <p>Your password has been reset.</p>
                            <table cellspacing="0" style="border: 2px solid #144daf; width: 300px; height: 200px;margin-left:15%; margin-top:2%;">
                                <tr><th>Your UserName - </th>' . $useremail . '<td></td></tr>
                                <tr style="background-color: #e0e0e0;"><th>Your Password - </th>' . $password . '<td></td></tr>
                            </table>
                             <h3>Thank You For Joining Us!</h3><br/>
                            <p>Kind Regards</p>
                            <p>FIA APP Team</p>
                        </body>
                        </html>';
      
        Yii::$app->mailer->compose()
                ->setFrom('fijiinstituteofacc@gmail.com')
                ->setTo($useremail)
                ->setSubject('FIA APP: User Credentials')
                ->setTextBody('Plain text content')
                ->setHtmlBody($htmlContent)
                ->send();
        
    }

    protected function sendUserInfos($useremail, $password) {
        $htmlContent = '<html>
                        <head><title>Welcome to FIA APP</title></head>
                        <body>
                            <h1>FIA MOBILE APP NOW AVAILABLE TO DOWNLOAD</h1>
                            <p>Ni sa bula Vinaka !</p>
                            <p>The Fiji Institute of Accountants is excited to offer our first ever mobile app for attendees to FIA Events.</p>
                            <p>The app will give you ability to view a wide variety of information about Congress, Technical Workshops & Seminars including general logistics, speaker details, session and schedule detail.</p>
                            <p>Download the FIA Fiji mobile app now:</p>
                            <a href="https://itunes.apple.com/us/app/fia-fiji/id1374664146?mt=8&ign-mpt=uo%3D4">
                            <img src="http://ec2-54-66-220-147.ap-southeast-2.compute.amazonaws.com/FIA_PORTAL/web/uploads/icons/apple.png" alt="Download App" width="150" height="40"></a>
                            
                            <a href="https://play.google.com/store/apps/details?id=com.itvtiapps.fia&rdid=com.itvtiapps.fia">
                            <img src="http://ec2-54-66-220-147.ap-southeast-2.compute.amazonaws.com/FIA_PORTAL/web/uploads/icons/androids.png" alt="Download App" width="150" height="40"></a>
                            
                            <table cellspacing="0" style="border: 2px solid #144daf; width: 300px; height: 200px;margin-left:15%; margin-top:2%;">
                                <tr><th>Your UserName - </th>' . $useremail . '<td></td></tr>
                                <tr style="background-color: #e0e0e0;"><th>Your Password - </th>' . $password . '<td></td></tr>
                            </table>
                             <h3>Thank You For Joining Us!</h3><br/>
                            <p>Kind Regards</p>
                            <p>FIA APP Team</p>
                        </body>
                        </html>';
      
        Yii::$app->mailer->compose()
                ->setFrom('fijiinstituteofacc@gmail.com')
                ->setTo($useremail)
                ->setSubject('FIA APP: User Credentials')
                ->setTextBody('Plain text content')
                ->setHtmlBody($htmlContent)
                ->send();
        
    }
    
    protected function random_string() {
        $length = 8;
        $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));

        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }

        return $key;
    }

}
