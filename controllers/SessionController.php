<?php

namespace app\controllers;

use Yii;
use app\api\modules\v1\models\Session;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\api\modules\v1\models\SessionDelegators;
use richardfan\sortable\SortableAction;

/**
 * SessionController implements the CRUD actions for Session model.
 */
class SessionController extends Controller {
    
     public function init() {
        $userid = Yii::$app->session['user_id'];
        if ($userid == '') {
            $this->redirect(Yii::$app->params['server'].Yii::$app->params['servername'].'/site/login');
        }
        parent::init();
    }


    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
    
    public function actions(){
    return [
        'sortItem' => [
            'class' => SortableAction::className(),
            'activeRecordClassName' => Session::className(),
            'orderColumn' => 'order',
        ],
        // your other actions
    ];
}

    /**
     * Lists all Session models.
     * @return mixed
     */
    public function actionIndex() {
        if (!empty($_POST)){
            
        $eventid = $_POST['eventid'];
        
        return $this->redirect(array('index', 
                    'eventid' => $eventid)
        );
        }
        return $this->render('index');
    }

    /**
     * Displays a single Session model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Session model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Session();

        if ($model->load(Yii::$app->request->post())) {
            // $transaction = Yii::$app->db->beginTransaction();
            //try {
            if ($model->save(false)) {
                
                if ($model->moderator_id !== '') {
                    
                    foreach ($model->moderator_id as $oneid) {
                        $moderatorobj = new SessionDelegators();
                        $moderatorobj->user_id = $oneid;
                        $moderatorobj->type = 'moderator';
                        $model->link('sessiondelegators', $moderatorobj);
                    }                    
                }
                if ($model->speaker_id !== '') {
                    foreach ($model->speaker_id as $oneid) {
                        $speakerobj = new SessionDelegators();
                        $speakerobj->user_id = $oneid;
                        $speakerobj->type = 'speaker';
                        $model->link('sessiondelegators', $speakerobj);
                    }
                }
                if ($_POST['Session']['gspeaker_id'] !== '') {
                    foreach ($_POST['Session']['gspeaker_id'] as $oneid) {
                        $gspeakerobj = new SessionDelegators();
                        $gspeakerobj->user_id = $oneid;
                        $gspeakerobj->type = 'guestspeaker';
                        $model->link('sessiondelegators', $gspeakerobj);
                    }
                }

                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                echo 'model not saved';
                exit;
            }
//                 echo 'model not saved';
//                 exit;
//            } catch (Exception $e) {
//
//                $transaction->rollBack();
            //   }
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Session model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            
            if ($model->save(false)) {
                
                SessionDelegators::deleteAll('session_id = :id', [':id' => $id]);
                
                if ($model->moderator_id !== '') {
                    
                    foreach ($model->moderator_id as $oneid) {
                        $moderatorobj = new SessionDelegators();
                        $moderatorobj->user_id = $oneid;
                        $moderatorobj->type = 'moderator';
                        $model->link('sessiondelegators', $moderatorobj);
                    }
                    
                }
                if ($model->speaker_id !== '') {
                    foreach ($model->speaker_id as $oneid) {
                        $speakerobj = new SessionDelegators();
                        $speakerobj->user_id = $oneid;
                        $speakerobj->type = 'speaker';
                        $model->link('sessiondelegators', $speakerobj);
                    }
                }
                if ($_POST['Session']['gspeaker_id'] !== '') {
                    foreach ($_POST['Session']['gspeaker_id'] as $oneid) {
                        $gspeakerobj = new SessionDelegators();
                        $gspeakerobj->user_id = $oneid;
                        $gspeakerobj->type = 'guestspeaker';
                        $model->link('sessiondelegators', $gspeakerobj);
                    }
                }

                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                echo 'model not saved';
                exit;
            }
            
        }  else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Session model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        
        Yii::$app->db->createCommand()->update('session', ['active' => 0], ['id' => $id,])->execute();
        //$this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Session model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Session the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Session::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
//    public function actionGeteventssess() {
//        $eventid = $_POST['eventid'];
//        
//        $sess = Session::getSessions($eventid);
////        var_dump($sess);
////        exit;
//        $sessdata = $this->renderPartial('_showeventsess', array('sess' => $sess), false, true);
//        
////        echo Json::encode($data);
////        exit;
//        return Json::encode($sessdata);
//    }
    

}
