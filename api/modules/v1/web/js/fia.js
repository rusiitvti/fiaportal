/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    //alert('why');
    $('#ajaxloaded').on('click', '#btn-assignUser', function () {
        var eventsid = $('#eventsatendees-events_id').val();
        //alert(eventsid);
        var userids = getRows();//$('#usergrid').yiiGridView('getSelectedRows');
        //alert(eventsid);
        $.ajax({
            url: 'assignevent',
            type: 'post',
            dataType: 'json',
            data: {
                eventid: eventsid,
                users: userids,
                _csrf: '<?=Yii::$app->request->getCsrfToken()?>'
            },
            success: function (data) {
                alert(data);
            },
            error: function (errormessage) {

                //do something else
                alert("User added to this event");

            }
        });
    });

    function getRows()
    {
        var strvalue = [];
        $('input[name="selection[]"]:checked').each(function () {
            if (strvalue !== "")
                strvalue.push(this.value);
           
        });
        return strvalue;
    }


    $('#btn-removeuser').on('click', function () {
        var eventsid = $('#eventsatendees-events_id').val();
        var userids = $('#usergrid').yiiGridView('getSelectedRows');
        $.ajax({
            url: 'deactivateuser',
            type: 'post',
            dataType: 'json',
            data: {
                eventid: eventsid,
                users: userids,
                _csrf: '<?=Yii::$app->request->getCsrfToken()?>'
            },
            success: function (data) {
                alert(data);
            },
            error: function (errormessage) {

                //do something else
                alert("User removed from this event");

            }
        });
    });

    $('#btn-assignSponsors').on('click', function () {
        var eventsid = $('#eventssponsors-events_id').val();
        var sponsorsid = $('#sponsorgrid').yiiGridView('getSelectedRows');
        $.ajax({
            url: 'transfersponsor',
            type: 'post',
            dataType: 'json',
            data: {
                eventid: eventsid,
                sponsors: sponsorsid,
                _csrf: '<?=Yii::$app->request->getCsrfToken()?>'
            },
            success: function (data) {
                alert(data);
            },
            error: function (errormessage) {

                //do something else
                alert("Sponsor added to this event");

            }
        });
    });

    $('#btn-activate').on('click', function () {
        var userids = $('#usergrid').yiiGridView('getSelectedRows');
        $.ajax({
            url: 'activate',
            type: 'post',
            dataType: 'json',
            data: {
                users: userids,
                _csrf: '<?=Yii::$app->request->getCsrfToken()?>'
            },
            success: function (data) {
                alert(data);
            },
            error: function (errormessage) {

                //do something else
                alert("User activated");

            }
        });
    });


    $('#eventsatendees-events_id').on('change', function () {
        var eventid = $('#eventsatendees-events_id').val();
        $.ajax({
            url: 'getnotusers',
            type: 'post',
            dataType: 'json',
            data: {
                eventid: eventid,
                _csrf: '<?=Yii::$app->request->getCsrfToken()?>'
            },
            success: function (data) {
//                var loadtemp = $.parseJSON(data);
//                alert(data);
                $('#ajaxloaded').html(data);
            },
            error: function (errormessage) {
                // alert(errormessage);
                //do something else
                //alert("Error getting users of this event");

            }
        });


    });

});
