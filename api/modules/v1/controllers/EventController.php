<?php

namespace app\api\modules\v1\controllers;

use Yii;
use app\api\modules\v1\models\Events;
use app\api\modules\v1\models\EventsAtendees;
use app\api\modules\v1\models\SearchEvents;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EventController implements the CRUD actions for Events model.
 */
class EventController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Events models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SearchEvents();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Events model.
     * @param integer $id
     * @param integer $venue_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id, $venue_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id, $venue_id),
        ]);
    }

    /**
     * Creates a new Events model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Events();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id, 'venue_id' => $model->venue_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Events model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @param integer $venue_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id, $venue_id)
    {
        $model = $this->findModel($id, $venue_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id, 'venue_id' => $model->venue_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Events model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @param integer $venue_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id, $venue_id)
    {
        $this->findModel($id, $venue_id)->delete();

        return $this->redirect(['index']);
    }


//      public function actionAssignuser() {

//         $model = new EventsAtendees();

//         if ($model->load(Yii::$app->request->post()) && $model->validate())

//             {
// // valid data received in $model
// // do something meaningful here about $model ...
//         return $this->render('assign', ['model' => $model]);
//             } else {
// // either the page is initially displayed or there is some
//             validation error
//             return $this->render('index', ['model' => $model]);
//             }
//         }
// }

//         //  if (!empty($_POST)){
            
//         // $eventid = $_POST['event_id'];
        
//         // return $this->redirect(array('assign', 
//         //             'eventid' => $eventid)
//         // );
//         // }
//         // return $this->render('assign');

//         //return $this->render('assign');
//     }

    /**
     * Finds the Events model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @param integer $venue_id
     * @return Events the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $venue_id)
    {
        if (($model = Events::findOne(['id' => $id, 'venue_id' => $venue_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
