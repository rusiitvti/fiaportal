<?php

namespace app\api\modules\v1\controllers;

use yii\rest\ActiveController;
use Yii;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\web\Controller;
use app\api\modules\v1\models\Events;
use app\api\modules\v1\models\User;
use app\api\modules\v1\models\EventsAtendees;
use app\api\modules\v1\models\Sponsors;
use app\api\modules\v1\models\Venue;
use app\api\modules\v1\models\Session;
use app\api\modules\v1\models\SessionDelegators;
use app\api\modules\v1\models\EventsSponsors;
use app\api\modules\v1\models\Reviews;
use app\api\modules\v1\models\PanelDiscussion;
use app\api\modules\v1\models\Notification;

class RestController extends Controller {

    public $modelClass = 'app\models\User';

    public function behaviors() {
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
        ];

        return $behaviors;
    }

    //to - do authenticate first
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionGet() {
        echo 'this is rest controller';
    }

    //get all books in database
    public function actionGetallevents() {
        \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
        //   $events = Events::find()->with('sessions')->all();
        $events = Events::find()->asArray()->all();

        //  ->joinWith('session')
        //   ->all(); //->where(['id' => '1'])->one();
        if (count($events) > 0) {
            $temp = [];
            foreach ($events as $oneevent) {
                $events = Events::findOne($oneevent['id']);
                $eventid = $oneevent['id'];
                unset($oneevent['venue_id']);
                //  $location = Venue::findOne($condition)
                $oneevent['venue'] = $events->venue->name;
                $oneevent['venue_address'] = $events->venue->address;
                $oneevent['lat'] = $events->venue->latitude;
                $oneevent['long'] = $events->venue->longtitude;
                $oneevent['session'] = Events::getAllSessions($eventid);

                //  $oneevent['attendees'] = $events->eventattendees;
                array_push($temp, $oneevent);
            }
            return array('status' => true, 'data' => $temp);
        } else {

            return array('status' => false, 'data' => 'No Events Found');
        }
    }

    /*
     * Get all attendees
     * to-do Get the event id and only return attendees of that Event only
     */

    public function actionGetattendees() {
        \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
        //   $events = Events::find()->with('sessions')->all();
        $events = EventsAtendees::find()->orderBy([
                            'user_id' => SORT_ASC])
                        ->asArray()->all();

        if (count($events) > 0) {
            $temp = [];
            foreach ($events as $oneevent) {
                $user = User::findOne($oneevent['user_id']);
                $user['password'] = '';
                $user['active'] = '';
                $oneevent['userinfo'] = $user;
                //  $oneevent['attendees'] = $events->eventattendees;
                array_push($temp, $oneevent);
            }
            return array('status' => true, 'data' => $temp);
        } else {

            return array('status' => false, 'data' => 'No Events Found');
        }
    }

    /*
     * validate user check if username and password match
     * then check if its a valid user
     */

    public function actionCheckuser() {
        \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
//        var_dump($_POST);
//        exit;
        if (isset($_POST['username'])) {
            $username = $_POST['username'];
            $password = $_POST['password'];
            $userdetails = User::validateuser($username, $password);
            if (count($userdetails) > 0) {
                return array('status' => 'true', 'data' => $userdetails);
            } else {
                return array('status' => 'false', 'data' => '');
            }
        } else {
            return array('status' => 'false', 'data' => '');
        }
    }
    
    public function actionGetuser(){
        
        \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
        
        if(isset($_POST['userid'])){
            
            $userid = $_POST['userid'];
            $user = User::findOne($userid);
            
            $user->password = '';
            //$userdetails = $user;
//            var_dump($user);
//            exit;            
            if (count($user) > 0) {
                return array('status' => 'true', 'data' => $user);
            } else {
                return array('status' => 'false', 'data' => '');
            }
            
            //return $user;
        }
        
    }

    public function actionRate() {
        \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;

        if (isset($_POST['type'])) {
            $result = '';
            $reviewtype = $_POST['type'];
            switch ($reviewtype) {
                case 'session' :
                    $sessionid = $_POST['sessionid'];
                    $userid = $_POST['userid'];
                    $countstar = $_POST['rate'];
                    $message = $_POST['message'];
                    $result = Reviews::addReviews('session', $sessionid, $userid, $countstar, $message);
                    break;
                case 'speaker' :
                    $speakerid = $_POST['speakerid'];
                    $userid = $_POST['userid'];
                    $countstar = $_POST['rate'];
                    $message = $_POST['message'];
                    $result = Reviews::addReviews('speaker', $speakerid, $userid, $countstar, $message);
                    break;
                case 'event' :
                    $eventid = $_POST['eventid'];
                    $userid = $_POST['userid'];
                    $countstar = $_POST['rate'];
                    $message = $_POST['message'];
                    $result = Reviews::addReviews('event', $eventid, $userid, $countstar, $message);
                    break;
            }
            return array('status' => 'true', 'data' => $result);
        } else {
            return array('status' => 'false', 'data' => 'Error Adding Reviews');
        }
    }

    public function actionGetspeakers() {
        \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
        $allatendees = SessionDelegators::find()->where([
                    'type' => 'speaker'
                ])->orderBy([
                    'user_id' => SORT_ASC])->asArray()->all();

        if (count($allatendees) > 0) {
            $temp = [];
            $userids = [];
            foreach ($allatendees as $oneattendee) {
                $temparr = [];
                $user = User::findOne($oneattendee['user_id']);
                $session = Session::findOne($oneattendee['session_id']);
                $events = Events::findOne($session->events_id);
                $user['password'] = '';
                
                $fname=$user['first_name'];
                
                $firstname = $fname ." ";
                
                $user['first_name']= $firstname;
                // $user['active'] = '';
                $arrkey = $oneattendee['user_id'] . $events->id;
                if (!array_key_exists($arrkey, $userids)) {
                    
                    $userids[$arrkey] = $oneattendee['user_id'];
                    $temparr['eventname'] = $events->name;
                    $temparr['eventid'] = $events->id;
                    // $temparr['type'] = 'Speaker';
                    $temparr['speaker'] = $user;
                    //  $oneevent['attendees'] = $events->eventattendees;
                    array_push($temp, $temparr);
                }
            }
            return array('status' => true, 'data' => $temp);
        } else {

            return array('status' => false, 'data' => 'No Speakers Found');
        }
    }

    /*
     * Get sponsors
     */

    public function actionGetsponsors() {
        \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
        //   $events = Events::find()->with('sessions')->all();
        // $sponsors = Sponsors::find()->asArray()->all();
        $eventssponsors = EventsSponsors::find()->asArray()->all();

        if (count($eventssponsors) > 0) {
            $temp = [];
            foreach ($eventssponsors as $oneevent) {
                $temparr = [];
                $temparr['eventid'] = $oneevent['events_id'];
                //$user = User::findOne($oneevent['user_id']);
                $temparr['sponsor'] = Sponsors::findOne($oneevent['sponsors_id']);
                //  $oneevent['attendees'] = $events->eventattendees;
                array_push($temp, $temparr);
            }
            return array('status' => true, 'data' => $temp);
        } else {

            return array('status' => false, 'data' => 'No Events Found');
        }
    }

    public function actionGetvenues() {
        $venues = Venue::find()->asArray()->all();
        $this->setHeader(200);
        echo json_encode(array('status' => 1, 'data' => $venues), JSON_PRETTY_PRINT);
    }

    public function actionPanels() {
        if (isset($_GET['sessionid'])) {
            $this->layout = 'loginLayout';
            $sessionid = $_GET['sessionid'];
            $this->layout = 'loginLayout';
            $model = new PanelDiscussion();

            if ($model->load(Yii::$app->request->post())) {
                $sessionid = $_POST['sessionid'];
                if ($model->save()) {
                    return $this->redirect(['view', 'id' => $model->id, 'session' => $sessionid]);
                }
            }

            return $this->render('create', [
                        'model' => $model, 'sessionid' => $sessionid
            ]);
        }
    }

    public function actionView($id, $session) {
        $this->layout = 'loginLayout';
        return $this->render('view', [
                    'model' => $this->findModel($id),
                    'session' => $session
        ]);
    }

    protected function findModel($id) {
        if (($model = PanelDiscussion::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionGetnotifications() {
        \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;

        $notifications = Notification::find()->select(['id, title, body, sent, date_format( date_add(lastmodified, INTERVAL 12 HOUR), "%Y-%m-%d %r") as lastmodified'])->asArray()->all();
        
        if (count($notifications) > 0) {
            return array('status' => true, 'data' => $notifications);
        } else {
            return array('status' => false, 'data' => 'No Notification Found');
        }
    }

    private function setHeader($status) {

        $status_header = 'HTTP/1.1 ' . $status . ' ' . $this->_getStatusCodeMessage($status);
        $content_type = "application/json; charset=utf-8";

        header($status_header);
        header('Content-type: ' . $content_type);
    }

    private function _getStatusCodeMessage($status) {
        // these could be stored in a .ini file and loaded
        // via parse_ini_file()... however, this will suffice
        // for an example
        $codes = Array(
            200 => 'OK',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
        );
        return (isset($codes[$status])) ? $codes[$status] : '';
    }
    
    

}
