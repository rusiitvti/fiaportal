<?php

namespace app\api\modules\v1\controllers;

use Yii;
use app\api\modules\v1\models\Session;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\api\modules\v1\models\PanelDiscussion;

/**
 * SessionController implements the CRUD actions for Session model.
 */
class SessionController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Session models.
     * @return mixed
     */
    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => Session::find(),
        ]);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Session model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Session();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Session model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Session model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Session model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Session the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionPanels() {
        if (isset($_GET['sessionid'])) {
            $this->layout = 'loginLayout';
            $sessionid = $_GET['sessionid'];
            $this->layout = 'loginLayout';
            $model = new PanelDiscussion();

            if ($model->load(Yii::$app->request->post())) {
                $sessionid = $_POST['sessionid'];
                if ($model->save()) {
                    return $this->redirect(['view', 'id' => $model->id, 'session' => $sessionid]);
                }
            }

            return $this->render('create', [
                        'model' => $model, 'sessionid' => $sessionid
            ]);
        }
    }

    public function actionView($id, $session) {
        $this->layout = 'loginLayout';
        return $this->render('view', [
                    'model' => $this->findModel($id),
                    'session' => $session
        ]);
    }

    protected function findModel($id) {
        if (($model = PanelDiscussion::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
