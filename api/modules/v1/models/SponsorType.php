<?php

namespace app\api\modules\v1\models;

use Yii;

/**
 * This is the model class for table "sponsortype".
 *
 * @property integer $id
 * @property string $type
 *
 * @property Sponsors[] $sponsors
 */
class SponsorType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sponsortype';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSponsors()
    {
        return $this->hasMany(Sponsors::className(), ['sponsortype_id' => 'id']);
    }
}
