<?php

namespace app\api\modules\v1\models;

use Yii;

/**
 * This is the model class for table "eventreviews".
 *
 * @property integer $events_id
 * @property integer $reviews_id
 *
 * @property Events $events
 * @property Reviews $reviews
 */
class EventsReviews extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'eventreviews';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['events_id', 'reviews_id'], 'required'],
            [['events_id', 'reviews_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'events_id' => 'Events ID',
            'reviews_id' => 'Reviews ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvents() {
        return $this->hasOne(Events::className(), ['id' => 'events_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReviews() {
        return $this->hasOne(Reviews::className(), ['id' => 'reviews_id']);
    }

    public function getEventreviews() {
        $allevents = Events::find()->where(['active' => '1'])->asArray()->all();
        if (count($allevents) > 0) {
            foreach ($allevents as $oneevent) {
                $eventid = $oneevent['id'];
                $eventname = $oneevent['name'];
                $review_result = $this->countReview($eventid,$eventname);
               // var_dump();
            }
         //   exit;
        }
      return json_encode($review_result);
    }

    private function countReview($eventid,$eventname) {
        $allreviews = EventsReviews::find()->where(['events_id' => $eventid])->andWhere(['active' => '1'])->asArray()->all();
       
        if (count($allreviews) > 0) {
            $totalnum = count($allreviews);
            $totalrate = 0;
            foreach ($allreviews as $onereview) {
                $reviewsid = $onereview['reviews_id'];
                $review = Reviews::find()->where(['id' => $reviewsid])->asArray()->all();
//                var_dump($review);
//                exit;
                $totalrate += $review[0]['ratings'];
            }
//            echo $totalnum;
//            echo $totalrate;
//            exit;
            $avgresult = $totalrate / $totalnum;
           
            $resultarr = [
                'eventid' => $eventid,
                'name' => $eventname,
                'avg' => $avgresult
            ];
            
            return $resultarr;
        }else{
            $resultarr = [
                'eventid' => $eventid,
                'name' => $eventname,
                 'avg' => 0
            ];
            return $resultarr;
        }
    }

}
