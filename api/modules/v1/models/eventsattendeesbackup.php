<?php

namespace app\api\modules\v1\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "eventattendees".
 *
 * @property integer $events_id
 * @property integer $user_id
 *
 * @property Events $events
 * @property User $user
 */
class EventsAtendees extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'eventattendees';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['events_id', 'user_id'], 'required'],
            [['events_id', 'user_id'], 'integer'],
            [['active'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'events_id' => 'Events Name',
            'user_id' => 'User ID',
            'active' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvents() {
        return $this->hasOne(Events::className(), ['id' => 'events_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getUsers() {
//        $listCategory = User::find()->select('first_name,last_name')
//                ->where(['active' => '0'])
//                ->all();
        $query = User::find()->where(['active' => '0']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        //  $users = User::find()->where(['active' => '0'])->all();
        // $list = ArrayHelper::map($users, 'id', 'first_name');
        // var_dump($users);
        //  exit;
        return $dataProvider;
    }

    public static function assignUsers($users) {
        $count = Yii::$app->db->createCommand()->batchInsert('eventattendees', ['events_id', 'user_id'], $users)->execute();
        if ($count > 0) {
            return 'Users Assigned Success!!';
        } else {
            return 'Error Assigned Users!!';
        }
    }

    public static function deactivateAttendees($eventsid, $userid) {
        $model = EventsAtendees::find()
                ->where(['events_id' => $eventsid])
                ->andWhere(['user_id' => $userid]);

        $model->active = 0;
        var_dump($model->errors);
        exit;
        $model->save();
        return 'true';
    }

}
