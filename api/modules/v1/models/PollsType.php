<?php

namespace app\api\modules\v1\models;

use Yii;

/**
 * This is the model class for table "pollstype".
 *
 * @property int $id
 * @property string $answer
 * @property string $lastmodified
 *
 * @property PollsquestionAnswer[] $pollsquestionAnswers
 * @property Polls[] $pollsquestions
 * @property PollsuserAnswers[] $pollsuserAnswers
 */
class PollsType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pollstype';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['lastmodified'], 'safe'],
            [['answer'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'answer' => 'Answer',
            'lastmodified' => 'Lastmodified',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPollsquestionAnswers()
    {
        return $this->hasMany(PollsquestionAnswer::className(), ['pollsanswerid' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPollsquestions()
    {
        return $this->hasMany(Polls::className(), ['id' => 'pollsquestionid'])->viaTable('pollsquestion_answer', ['pollsanswerid' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPollsuserAnswers()
    {
        return $this->hasMany(PollsuserAnswers::className(), ['pollstypeid' => 'id']);
    }
}
