<?php

namespace app\api\modules\v1\models;

use Yii;

/**
 * This is the model class for table "notification".
 *
 * @property integer $id
 * @property string $title
 * @property string $body
 * @property integer $sent
 * @property string $lastmodified
 *
 * @property Eventsnotification[] $eventsnotifications
 * @property Sessionnotification[] $sessionnotifications
 */
class Notification extends \yii\db\ActiveRecord {

    public $eventid;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'notification';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['sent', 'eventid'], 'integer'],
            [['eventid', 'title', 'body'], 'required'],
            [['lastmodified'], 'safe'],
            [['title'], 'string', 'max' => 45],
            [['body'], 'string', 'max' => 400]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'body' => 'Body',
            'sent' => 'Sent',
            'eventid' => 'Event',
            'lastmodified' => 'Lastmodified',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventsnotifications() {
        return $this->hasMany(Eventsnotification::className(), ['notification_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSessionnotifications() {
        return $this->hasMany(Sessionnotification::className(), ['notification_id' => 'id']);
    }

}
