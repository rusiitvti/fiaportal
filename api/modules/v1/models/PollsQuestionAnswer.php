<?php

namespace app\api\modules\v1\models;

use Yii;

/**
 * This is the model class for table "pollsquestion_answer".
 *
 * @property int $pollsquestionid
 * @property int $pollsanswerid
 * @property int $sessionid
 * @property string $lastmodified
 *
 * @property Session $session
 * @property Pollstype $pollsanswer
 * @property Polls $pollsquestion
 */
class PollsQuestionAnswer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pollsquestion_answer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pollsquestionid', 'pollsanswerid'], 'required'],
            [['pollsquestionid', 'pollsanswerid', 'sessionid'], 'integer'],
            [['lastmodified'], 'safe'],
            [['pollsquestionid', 'pollsanswerid'], 'unique', 'targetAttribute' => ['pollsquestionid', 'pollsanswerid']],
            [['sessionid'], 'exist', 'skipOnError' => true, 'targetClass' => Session::className(), 'targetAttribute' => ['sessionid' => 'id']],
            [['pollsanswerid'], 'exist', 'skipOnError' => true, 'targetClass' => Pollstype::className(), 'targetAttribute' => ['pollsanswerid' => 'id']],
            [['pollsquestionid'], 'exist', 'skipOnError' => true, 'targetClass' => Polls::className(), 'targetAttribute' => ['pollsquestionid' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pollsquestionid' => 'Pollsquestionid',
            'pollsanswerid' => 'Pollsanswerid',
            'sessionid' => 'Sessionid',
            'lastmodified' => 'Lastmodified',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSession()
    {
        return $this->hasOne(Session::className(), ['id' => 'sessionid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPollsanswer()
    {
        return $this->hasOne(Pollstype::className(), ['id' => 'pollsanswerid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPollsquestion()
    {
        return $this->hasOne(Polls::className(), ['id' => 'pollsquestionid']);
    }
}
