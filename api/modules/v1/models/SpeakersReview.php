<?php

namespace app\api\modules\v1\models;


use Yii;
use app\api\modules\v1\models\EventsAtendees;

/**
 * This is the model class for table "speakersreview".
 *
 * @property integer $reviews_id
 * @property integer $speaker_id
 *
 * @property Reviews $reviews
 * @property User $speaker
 */
class SpeakersReview extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'speakersreview';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['reviews_id', 'speaker_id'], 'required'],
            [['reviews_id', 'speaker_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'reviews_id' => 'Reviews ID',
            'speaker_id' => 'Speaker ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReviews() {
        return $this->hasOne(Reviews::className(), ['id' => 'reviews_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpeaker() {
        return $this->hasOne(User::className(), ['id' => 'speaker_id']);
    }

    public function getRatingsall($eventsponors) {


        $alleventatt = Eventsatendees::find()->select('user_id')->where(['events_id' => $eventsponors]);



        $allatendees = SessionDelegators::find()->select(['user_id'])
                        ->andWhere(['type' => 'speaker'])
                        ->andWhere(['IN','user_id',$alleventatt])
                        ->distinct()
                        ->orderBy(['user_id' => SORT_ASC])->asArray()->all();
        $temparr = [];
        $avg = 0;
        if (count($allatendees) > 0) {
            foreach ($allatendees as $oneattendee) {
                $speaker = [];
                $user = User::findOne($oneattendee['user_id']);
                $user['password'] = '';
                $allreviews = SpeakersReview::find()
                                ->select(['reviews_id'])
                                ->where(['speaker_id' => $oneattendee['user_id']])
                                ->asArray()->all();
                if (count($allreviews) > 0) {
                    $count = count($allreviews);
                    $total = 0;
                    foreach ($allreviews as $reviewid) {
                        $ratins = Reviews::find()->select(['ratings'])->where(['id' => $reviewid])->one();
                        $intval = intval($ratins['ratings']);
                        $total += $intval;
                    }
                    $tempavg = $total / $count;
                    //$avg = number_format((float) $tempavg, 2, '.', '');
                    $avg = round($tempavg);
                }
                $speaker['name'] = $user['picture'];
                $speaker['pic'] = $user['first_name'];
                $speaker['avg'] = $avg;
                array_push($temparr, $speaker);
            }
        }
//        var_dump($temparr);
//        exit;
        return $temparr;
    }

}
