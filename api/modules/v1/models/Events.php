<?php

namespace app\api\modules\v1\models;

use Yii;

/**
 * This is the model class for table "events".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $start_date
 * @property string $end_date
 * @property int $active
 * @property string $lastmodified
 * @property string $image_cover
 * @property int $venue_id
 *
 * @property Eventattendees[] $eventattendees
 * @property Venue $venue
 * @property Eventsnotification[] $eventsnotifications
 * @property Eventssponsors[] $eventssponsors
 * @property Polls[] $polls
 * @property Session[] $sessions
 */
class Events extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public $file;
    public $sessionList;
    public $attendesList;
    public $sponsorList;
    public $event;
    public $event_id;

//    function __construct() {
//        //  $id = $this->id;
//        $this->getSponsors();
//    }

    public static function tableName() {
        return 'events';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['start_date', 'sponsorList', 'end_date'], 'safe'],
            [['name', 'venue_id', 'start_date', 'end_date', 'image_cover', 'description'], 'required'],
            [['file'], 'file'],
            // [['name', 'image_cover'], 'string', 'max' => 45],            
            //[['description'], 'string', 'max' => 250],
            [['venue_id'], 'exist', 'skipOnError' => true, 'targetClass' => Venue::className(), 'targetAttribute' => ['venue_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'Choose Event',
            'name' => 'Event Name',
            'description' => 'Event Description',
            'start_date' => 'Event Start Date',
            'end_date' => 'Event End Date',
            // 'active' => 'Active',
            //  'lastmodified' => 'Lastmodified',
            'image_cover' => 'Image Cover',
            'venue_id' => 'Event Venue',
            'file' => 'Event Cover Image',
            'sponsorList' => 'Sponsor List',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventattendees() {
        return $this->hasMany(Eventattendees::className(), ['events_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventreviews() {
        return $this->hasMany(Eventreviews::className(), ['events_id' => 'id']);
    }

    public function getVenue() {
        return $this->hasOne(Venue::className(), ['id' => 'venue_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventsnotifications() {
        return $this->hasMany(Eventsnotification::className(), ['events_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventssponsors() {
        return $this->hasMany(Eventssponsors::className(), ['events_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSessions() {
        return $this->hasMany(Session::className(), ['events_id' => 'id']);
    }

    public function getSponsors() {
        $sponsorlist = '';
        $eventsid = $this->id;
        //  echo $eventsid;
        $listofspeakers = EventsSponsors::find()
                        ->where(['events_id' => $eventsid])
                        ->asArray()->all();
//        var_dump($listofspeakers);
//        exit;
        foreach ($listofspeakers as $arow) {
            $sponsorid = $arow['sponsors_id'];

            $sponsor = Sponsors::find()
                    ->where(['id' => $sponsorid])
                    ->asArray()
                    ->all();

            $sponsorlist .= $sponsor[0]['companyname'] . ' , ';
            // var_dump($sponsor);
            //exit;
        }
        // echo $sponsorlist;
        //exit;
        return $sponsorlist;
        //$this->sponsorList = $sponsorlist;
    }

    public static function getAllSessions($eventid) {
        $allsessions = Session::find()->where(['events_id' => $eventid])->orderBy([
                    'order' => SORT_ASC
                    
                ])->asArray()->all();
        $tempsession = [];
        foreach ($allsessions as $asession) {
            $speakerarr = Session::getSpeakersName($asession['id']);
            //  if (count($speakerarr) > 0) {
            $peakername = $speakerarr['name'];
            $speakerid = $speakerarr['id'];
            $asession['speakername'] = $peakername;
            $asession['speakerid'] = $speakerid;
            //    }
            array_push($tempsession, $asession);
        }
        return $tempsession;
        //  exit;
    }

    public static function getNumevents() {
        $count = Events::find()->select(['COUNT(*) AS cnt'])->where(['active' => '1'])->asArray()->all();
        return $count[0]['cnt'];
    }

}
