<?php

namespace app\api\modules\v1\models;

use Yii;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;



/**
 * This is the model class for table "session".
 *
 * @property int $id
 * @property string $start_time
 * @property string $end_time
 * @property string $program
 * @property int $active
 * @property string $lastmodified
 * @property int $venue_id
 * @property int $events_id
 * @property string $session_date
 * @property int $hasdiscussion
 * @property int $order
 *
 * @property Feeds[] $feeds
 * @property Paneldiscussion[] $paneldiscussions
 * @property Polls[] $polls
 * @property PollsquestionAnswer[] $pollsquestionAnswers
 * @property PollsuserAnswers[] $pollsuserAnswers
 * @property Events $events
 * @property Venue $venue
 * @property Sessiondelegators[] $sessiondelegators
 * @property Sessionnotification[] $sessionnotifications
 * @property Sessionreview[] $sessionreviews
 */
class Session extends \yii\db\ActiveRecord
{
    
    public $speaker_id;
    public $moderator_id;
    public $gspeaker_id;
    public $event;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'session';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['events_id', 'venue_id', 'speaker_id', 'moderator_id','hasdiscussion','isdebate','order'], 'integer'],
            [['events_id', 'start_time', 'venue_id', 'session_date','hasdiscussion','isdebate'], 'required'],
            // [['session_date'], 'safe'],
            [['start_time', 'end_time', 'program'], 'string', 'max' => 250]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'start_time' => 'Start Time',
            'end_time' => 'End Time',
            'program' => 'Program',
            'session_date' => 'Session Date',
            // 'lastmodified' => 'Lastmodified',
            'speaker_id' => 'Speaker',
            'moderator_id' => 'Moderator',
            'gspeaker_id' => 'Guest Speaker',
            'events_id' => 'Event',
            'hasdiscussion' => 'Has Discussion?',
            'isdebate' => 'Has Polling?',
            'order' =>'Session Order Number',
            'venue_id' => 'Location of the Session',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeeds()
    {
        return $this->hasMany(Feeds::className(), ['session_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaneldiscussions()
    {
        return $this->hasMany(Paneldiscussion::className(), ['session_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPolls()
    {
        return $this->hasMany(Polls::className(), ['session' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPollsquestionAnswers()
    {
        return $this->hasMany(PollsquestionAnswer::className(), ['sessionid' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPollsuserAnswers()
    {
        return $this->hasMany(PollsuserAnswers::className(), ['sessionid' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvents()
    {
        return $this->hasOne(Events::className(), ['id' => 'events_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVenue()
    {
        return $this->hasOne(Venue::className(), ['id' => 'venue_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSessiondelegators()
    {
        return $this->hasMany(SessionDelegators::className(), ['session_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSessionnotifications()
    {
        return $this->hasMany(Sessionnotification::className(), ['session_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSessionreviews()
    {
        return $this->hasMany(Sessionreview::className(), ['session_id' => 'id']);
    }
    
    public function getSpeakerDropdown() {

        // ------- Get the id from usertype where description is speaker -----8
        $subquery = (new Query())->select('id')->from('usertype')->where(['description' => 'speaker']);
        $query = (new Query())->select('user_id')->from('userhastype')->where(['userType_id' => $subquery]);
        $rows = $query->all();

        // ------- Get all the user id where the user type is speaker
        $userlist = array();
        foreach ($rows as $onerow) {
            $userid = $onerow['user_id'];
            $userquery = (new Query())->select(['id', 'first_name', 'last_name'])->from('user')->where(['id' => $userid]);
            $userrow = $userquery->all();
            array_push($userlist, $userrow);
        }

        // ------- build your final array for the list to return
        $finallist = array();
        foreach ($userlist as $key => $value) {
            foreach ($value as $onerow) {
                $temp = ['id' => $onerow['id'], 'name' => $onerow['first_name'] . ' ' . $onerow['last_name']];
                // $list = ArrayHelper::map($value, 'id', 'first_name');
                array_push($finallist, $temp);
            }
        }
        $list = ArrayHelper::map($finallist, 'id', 'name');
//        var_dump($finallist);
//        exit;
        return $list;
    }
    
    public function getPanellistDropdown() {

        // ------- Get the id from usertype where description is speaker -----8
        $subquery = (new Query())->select('id')->from('usertype')->where(['description' => 'speaker']);
        $guestspeaker = (new Query())->select('id')->from('usertype')->where(['description' => 'guestspeaker']);
        $query = (new Query())->select('user_id')->from('userhastype')->where(['userType_id' => $subquery])->orWhere(['userType_id' => $guestspeaker]);
        $rows = $query->all();

        // ------- Get all the user id where the user type is speaker
        $userlist = array();
        foreach ($rows as $onerow) {
            $userid = $onerow['user_id'];
            $userquery = (new Query())->select(['id', 'first_name', 'last_name'])->from('user')->where(['id' => $userid]);
            $userrow = $userquery->all();
            array_push($userlist, $userrow);
        }

        // ------- build your final array for the list to return
        $finallist = array();
        foreach ($userlist as $key => $value) {
            foreach ($value as $onerow) {
                $temp = ['id' => $onerow['id'], 'name' => $onerow['first_name'] . ' ' . $onerow['last_name']];
                // $list = ArrayHelper::map($value, 'id', 'first_name');
                array_push($finallist, $temp);
            }
        }
        $list = ArrayHelper::map($finallist, 'id', 'name');
//        var_dump($finallist);
//        exit;
        return $list;
    }
    
    public function getGspeakerDropdown() {

        // ------- Get the id from usertype where description is speaker -----8
        $subquery = (new Query())->select('id')->from('usertype')->where(['description' => 'guestspeaker']);
        $query = (new Query())->select('user_id')->from('userhastype')->where(['userType_id' => $subquery]);
        $rows = $query->all();

        // ------- Get all the user id where the user type is speaker
        $userlist = array();
        foreach ($rows as $onerow) {
            $userid = $onerow['user_id'];
            $userquery = (new Query())->select(['id', 'first_name', 'last_name'])->from('user')->where(['id' => $userid]);
            $userrow = $userquery->all();
            array_push($userlist, $userrow);
        }

        // ------- build your final array for the list to return
        $finallist = array();
        foreach ($userlist as $key => $value) {
            foreach ($value as $onerow) {
                $temp = ['id' => $onerow['id'], 'name' => $onerow['first_name'] . ' ' . $onerow['last_name']];
                // $list = ArrayHelper::map($value, 'id', 'first_name');
                array_push($finallist, $temp);
            }
        }
        $list = ArrayHelper::map($finallist, 'id', 'name');
//        var_dump($finallist);
//        exit;
        return $list;
    }

    public function getModeratorDropdown() {

        // ------- Get the id from usertype where description is speaker -----8
        $subquery = (new Query())->select('id')->from('usertype')->where(['description' => 'moderator']);
        $query = (new Query())->select('user_id')->from('userhastype')->where(['userType_id' => $subquery]);
        $rows = $query->all();

        // ------- Get all the user id where the user type is speaker
        $userlist = array();
        foreach ($rows as $onerow) {
            $userid = $onerow['user_id'];
            $userquery = (new Query())->select(['id', 'first_name', 'last_name'])->from('user')->where(['id' => $userid]);
            $userrow = $userquery->all();
            array_push($userlist, $userrow);
        }

        // ------- build your final array for the list to return
        $finallist = array();
        foreach ($userlist as $key => $value) {
            foreach ($value as $onerow) {
                $temp = ['id' => $onerow['id'], 'name' => $onerow['first_name'] . ' ' . $onerow['last_name']];
                // $list = ArrayHelper::map($value, 'id', 'first_name');
                array_push($finallist, $temp);
            }
        }
        $list = ArrayHelper::map($finallist, 'id', 'name');
//        var_dump($finallist);
//        exit;
        return $list;
    }

    public function getSpeakers() {
        $sessionid = $this->id;
        $row = SessionDelegators::find()
                        ->where(['session_id' => $sessionid])
                        ->andWhere(['type' => 'speaker'])
                        ->asArray()->all();
        //  $query = (new Query())->select('user_id')->from('sessiondelegators')->where(['session_id' => $sessionid]);
        $name = '';
        if (sizeof($row) > 0) {
            foreach ($row as $onerow) {
                $userrow = User::find()
                                ->where(['id' => $onerow['user_id']])
                                ->asArray()->all();
                $name .= $userrow[0]['first_name'] . ' ' . $userrow[0]['last_name'] . ', ';
            }
        }
        $resul = rtrim(trim($name), ',');
        return $resul;
        //  var_dump($row);
    }

    public function getModerator() {
        $sessionid = $this->id;
        $row = SessionDelegators::find()
                        ->where(['session_id' => $sessionid])
                        ->andWhere(['type' => 'moderator'])
                        ->asArray()->all();
        //  $query = (new Query())->select('user_id')->from('sessiondelegators')->where(['session_id' => $sessionid]);
        $name = '';
        if (sizeof($row) > 0) {
            foreach ($row as $onerow) {
                $userrow = User::find()
                                ->where(['id' => $onerow['user_id']])
                                ->asArray()->all();
//       var_dump($userrow);
//       exit;
                $name = $userrow[0]['first_name'] . ' ' . $userrow[0]['last_name'];
            }
        }
        return $name;
        //  var_dump($row);
    }

    public static function getSpeakersName($sessionid) {
        // = $this->id;
        $row = SessionDelegators::find()
                        ->where(['session_id' => $sessionid])
                        ->andWhere(['type' => 'speaker'])
                        ->asArray()->all();
        //  $query = (new Query())->select('user_id')->from('sessiondelegators')->where(['session_id' => $sessionid]);
        $usr = [];
        if (sizeof($row) > 0) {

            foreach ($row as $onerow) {

                $userrow = User::find()
                                ->where(['id' => $onerow['user_id']])
                                ->asArray()->all();

                $name = $userrow[0]['first_name'] . ' ' . $userrow[0]['last_name'];
                $usr['id'] = $onerow['user_id'];
                $usr['name'] = $name;
            }
            return $usr;
        } else {
            $usr['id'] = '';
            $usr['name'] = '';
            return $usr;
        }

        //  var_dump($row);
    }

    public static function getSessionhead() {
        $row = Session::find()
                        ->where(['end_time' => ''])
                        ->asArray()->all();
        return $row;
    }


    public static function getSessionpanel($id) {
        $row = Session::find()
                        ->where(['id' => $id])
                        ->asArray()->all();
        return $row;
    }

    public static function getSpeakerpanel($id) {
        $sbsquery = SessionDelegators::find()->select('user_id')->where(['session_id' => $id])->andWhere(['type' => 'speaker']);
        $allspeakers = User::find()
                ->select(['first_name', 'id','last_name'])
                ->where(['IN', 'id', $sbsquery])->asArray()->all();
//        $dataProvider = new ActiveDataProvider([
//            'query' => $model,
//        ]);
         // ------- build your final array for the list to return
        $finallist = array();
        foreach ($allspeakers as $onerow) {
            //foreach ($value as $onerow) {
                $temp = ['id' => $onerow['id'], 'name' => $onerow['first_name'] . ' ' . $onerow['last_name']];
                // $list = ArrayHelper::map($value, 'id', 'first_name');
                array_push($finallist, $temp);
            //}
        }
        $list = ArrayHelper::map($finallist, 'id', 'name');

        return $list;
    }
    
//    public static function getSessions($eventid){
//        
//        $session = Session::find()->where(['active'=>1])->andWhere(['events_id'=>$eventid]);
//        
//        $dataProvider = new ActiveDataProvider([
//            'query' => $session,
//        ]);
//        
//        return $dataProvider;
//    }



}
