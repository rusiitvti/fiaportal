<?php

namespace app\api\modules\v1\models;

use Yii;
use app\api\modules\v1\models\Session;
/**
 * This is the model class for table "paneldiscussion".
 *
 * @property integer $id
 * @property string $question
 * @property integer $session_id
 * @property integer $user_id
 * @property integer $active
 * @property string $lastmodified
 *
 * @property Session $session
 * @property User $user
 */
class PanelDiscussion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'paneldiscussion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'session_id', 'user_id'], 'required'],
            [[ 'session_id', 'user_id', 'active'], 'integer'],
            [['lastmodified'], 'safe'],
            [['question'], 'string', 'max' => 1000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'question' => 'Question:',
            'session_id' => 'Session',
            'user_id' => 'Question for Who:',
            'active' => 'Active',
            'lastmodified' => 'Lastmodified',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSession()
    {
        return $this->hasOne(Session::className(), ['id' => 'session_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    
    public function getSessions($sessionid) {

        $row = Session::find()
                        ->where(['id' => $sessionid])
                        ->asArray()->all();
       
        $name = '';
        if (sizeof($row) > 0) {
            foreach ($row as $onerow) {
             
                $name = $onerow['program'];
            }
        }
        return $name;
    
    }
    public function getSpeakersname($userid) {
        $row = User::find()
                        ->where(['id' => $userid])
                        ->asArray()->all();
        $name = '';
        if (sizeof($row) > 0) {
                $name = $row[0]['first_name'] . ' ' . $row[0]['last_name'] . ' - ' . $row[0]['designation'] . ' of ' . $row[0]['company_name'];
            return $name;
        } else {
            return $name;
        }

    }
}