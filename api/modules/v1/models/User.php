<?php

namespace app\api\modules\v1\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $phone
 * @property string $picture
 * @property string $gender
 * @property string $company_name
 * @property string $designation
 * @property string $qualification
 * @property integer $active
 * @property string $lastmodified
 *
 * @property Eventattendees[] $eventattendees
 * @property Feeds[] $feeds
 * @property Paneldiscussion[] $paneldiscussions
 * @property Reviews[] $reviews
 * @property Sessiondelegators[] $sessiondelegators
 * @property Speakersreview[] $speakersreviews
 * @property Userhastype[] $userhastypes
 */
class User extends \yii\db\ActiveRecord {

    public $file;
    public $usertype = [];
    public $oldpassword;
    public $newpassword;
    public $confirmnewpass;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['first_name', 'email', 'last_name', 'gender', 'company_name', 'usertype'], 'required'],
            [['username'], 'unique'],
            [['password', 'oldpassword', 'newpassword', 'confirmnewpass','phone','designation','qualification'], 'string', 'max' => 200],
            //[['phone'], 'match', 'pattern' => '/^([0-9]( |-)?)?(\(?[0-9]{3}\)?|[0-9]{3})( |-)?([0-9]{3}( |-)?[0-9]{4}|[a-zA-Z0-9]{7})$/'],
            [['email'], 'email'],
            [['file'], 'file'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'password' => 'Password',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'email' => 'Email',
            'phone' => 'Phone',
            'gender' => 'Gender',
            'file' => 'User Cover Image',
            'usertype' => 'User Type',
            'company_name' => 'Company Name',
            'designation' => 'Designation',
            'qualification' => 'Qualification',
            'oldpassword' => 'Old Password',
            'newpassword' => 'New Password',
            'confirmnewpass' => 'Confirm New Password'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventattendees() {
        return $this->hasMany(Eventattendees::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeeds() {
        return $this->hasMany(Feeds::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaneldiscussions() {
        return $this->hasMany(Paneldiscussion::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReviews() {
        return $this->hasMany(Reviews::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSessiondelegators() {
        return $this->hasMany(Sessiondelegators::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpeakersreviews() {
        return $this->hasMany(Speakersreview::className(), ['speaker_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserhastypes() {
        return $this->hasMany(UserHasType::className(), ['user_id' => 'id']);
    }

    public function getUserDropdown() {
        $listCategory = UserType::find()->select('id,description')
                ->where(['active' => '1'])
                ->all();
        $list = ArrayHelper::map($listCategory, 'id', 'description');

        return $list;
    }

    public static function validateuser($username, $password) {
        $isvalid = 'false';
        $user = User::find()->where(['username' => $username])
                ->andWhere(['active' => '1'])
                ->asArray()
                ->all();
        $result = [];
        //first check if the user is activated
        if (count($user) > 0) {
            $passwordhash = $user[0]['password'];
            //then validate if the password passed is the same as the one in database
            if (Yii::$app->getSecurity()->validatePassword($password, $passwordhash)) {
                $isvalid = 'true';
                $user[0]['password'] = '';
                $result = $user;
            } else {
                return $result;
            }
        }
        return $result;
    }

    public function getNumusers() {
        $count = User::find()->select(['COUNT(*) AS cnt'])->where(['active' => '1'])->asArray()->all();
        return $count[0]['cnt'];
    }

}
