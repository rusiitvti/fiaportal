<?php

namespace app\api\modules\v1\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "userhastype".
 *
 * @property integer $user_id
 * @property integer $userType_id
 *
 * @property User $user
 * @property Usertype $userType
 */
class UserHasType extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'userhastype';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['user_id', 'userType_id'], 'required'],
            [['user_id', 'userType_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'user_id' => 'User ID',
            'userType_id' => 'User Type ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserType() {
        return $this->hasOne(Usertype::className(), ['id' => 'userType_id']);
    }

    public static function getUsertypes($userid) {
        $query = new \yii\db\Query;
        $query->select([
                    'usertype.description',
                        ]
                )
                ->from('userhastype')
                ->join('LEFT JOIN', 'usertype', 'userhastype.userType_id = usertype.id')
                ->WHERE(['userhastype.user_id' => $userid]);
        $command = $query->createCommand();
        $data = $command->queryAll();
        $status = '';
        if(count($data) > 0){
            foreach ($data as $atype){
                $usertype = $atype['description'];
                if($usertype == 'admin' || $usertype == 'moderator'){
                    $status .= 'allow';
                    break;
                }else{
                    continue;
                }
            }
        }

        return $status;
    }

}
