<?php

namespace app\api\modules\v1\models;

use Yii;
use yii\helpers\ArrayHelper;


/**
 * This is the model class for table "venue".
 *
 * @property integer $id
 * @property string $name
 * @property string $location
 * @property string $address
 * @property string $lastmodified
 * @property string $latitude
 * @property string $longtitude
 *
 * @property Events[] $events
 * @property Session[] $sessions
 */
class Venue extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'venue';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
           // [['lastmodified'], 'safe'],
            [['latitude', 'longtitude'], 'number'],
            [['name', 'location', 'address'], 'string', 'max' => 45],
            [['latitude', 'longtitude','name','address'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'location' => 'Location of the Venue',
            'address' => 'Address',
          //  'lastmodified' => 'Lastmodified',
            'latitude' => 'Latitude',
            'longtitude' => 'Longtitude',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvents()
    {
        return $this->hasMany(Events::className(), ['venue_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSessions()
    {
        return $this->hasMany(Session::className(), ['venue_id' => 'id']);
    }
     public function getLocations() {
        $listCategory = array(
           ['value' => 'Suva'],  ['value' => 'Nausori'],  ['value' => 'Lautoka'],  ['value' => 'Nadi'],  ['value' => 'Sigatoka'],  ['value' => 'Navua'],  ['value' => 'Labasa'],
            
        );
//        $list = ArrayHelper::map($listCategory, 'value');

        return $listCategory;
    }
}
