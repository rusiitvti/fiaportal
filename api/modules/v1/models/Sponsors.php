<?php

namespace app\api\modules\v1\models;

use Yii;
use app\api\modules\v1\models\SponsorType;

/**
 * This is the model class for table "sponsors".
 *
 * @property int $id
 * @property string $companyname
 * @property string $logo
 * @property string $lastmodified
 * @property int $sponsortype_id
 * 
 * 
 * @property int $active
 * @property string $name
 * @property string $url
 *
 * @property Eventssponsors[] $eventssponsors
 * @property Sponsortype $sponsortype
 */
class Sponsors extends \yii\db\ActiveRecord {

    public $file;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'sponsors';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['sponsortype_id', 'active'], 'integer'],
            [['lastmodified'], 'safe'],
            [['companyname', 'logo'], 'string', 'max' => 150],
            [['url'], 'string', 'max' => 150],
            [['sponsortype_id'], 'exist', 'skipOnError' => true, 'targetClass' => Sponsortype::className(), 'targetAttribute' => ['sponsortype_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'companyname' => 'Company Name',
            'logo' => ' Select Logo',
            //'file' => 'Select Logo',
            'lastmodified' => 'Lastmodified',
            'sponsortype_id' => 'Sponsor Type',
            'active' => 'Active',
            // 'name' => 'Name',
            'url' => 'Business Url',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventssponsors() {
        return $this->hasMany(Eventssponsors::className(), ['sponsors_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSponsortype() {
        return $this->hasOne(Sponsortype::className(), ['id' => 'sponsortype_id']);
    }

    public function getTypes() {
        $sponsortypeid = $this->sponsortype_id;
        $row = SponsorType::find()
                        ->where(['id' => $sponsortypeid])
                        //->andWhere(['type'=>'moderator'])
                        ->asArray()->all();
        //  $query = (new Query())->select('user_id')->from('sessiondelegators')->where(['session_id' => $sessionid]);
        $sponsortype = $row[0]['type'];
        return $sponsortype;
    }
    
       


}
