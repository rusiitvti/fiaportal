<?php

namespace app\api\modules\v1\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\api\modules\v1\models\Sponsors;

/**
 * SearchSponsors represents the model behind the search form of `app\api\modules\v1\models\Sponsors`.
 */
class SearchSponsors extends Sponsors
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'sponsortype_id', 'active'], 'integer'],
            [['companyname', 'logo', 'lastmodified', 'url'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Sponsors::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'lastmodified' => $this->lastmodified,
            'sponsortype_id' => $this->sponsortype_id,
            'active' => $this->active,
        ]);

        $query->andFilterWhere(['like', 'companyname', $this->companyname])
            ->andFilterWhere(['like', 'logo', $this->logo])
          //  ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'url', $this->url]);

        return $dataProvider;
    }
}
