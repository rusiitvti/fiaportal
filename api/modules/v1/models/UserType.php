<?php

namespace app\api\modules\v1\models;

use Yii;

/**
 * This is the model class for table "usertype".
 *
 * @property integer $id
 * @property string $description
 * @property integer $active
 *
 * @property Userhastype[] $userhastypes
 */
class UserType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'usertype';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['active'], 'integer'],
            [['description'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'description' => 'Description',
            'active' => 'Active',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserhastypes()
    {
        return $this->hasMany(Userhastype::className(), ['userType_id' => 'id']);
    }
}
