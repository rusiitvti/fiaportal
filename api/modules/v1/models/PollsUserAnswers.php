<?php

namespace app\api\modules\v1\models;

use Yii;

/**
 * This is the model class for table "pollsuser_answers".
 *
 * @property int $id
 * @property int $pollsid
 * @property int $pollstypeid
 * @property int $userid
 * @property int $sessionid
 * @property string $lastmodified
 *
 * @property Session $session
 * @property Polls $polls
 * @property Pollstype $pollstype
 * @property User $user
 */
class PollsUserAnswers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pollsuser_answers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pollsid', 'pollstypeid', 'userid'], 'required'],
            [['pollsid', 'pollstypeid', 'userid', 'sessionid'], 'integer'],
            [['lastmodified'], 'safe'],
            [['sessionid'], 'exist', 'skipOnError' => true, 'targetClass' => Session::className(), 'targetAttribute' => ['sessionid' => 'id']],
            [['pollsid'], 'exist', 'skipOnError' => true, 'targetClass' => Polls::className(), 'targetAttribute' => ['pollsid' => 'id']],
            [['pollstypeid'], 'exist', 'skipOnError' => true, 'targetClass' => Pollstype::className(), 'targetAttribute' => ['pollstypeid' => 'id']],
            [['userid'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['userid' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pollsid' => 'Pollsid',
            'pollstypeid' => 'Pollstypeid',
            'userid' => 'Userid',
            'sessionid' => 'Sessionid',
            'lastmodified' => 'Lastmodified',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSession()
    {
        return $this->hasOne(Session::className(), ['id' => 'sessionid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPolls()
    {
        return $this->hasOne(Polls::className(), ['id' => 'pollsid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPollstype()
    {
        return $this->hasOne(Pollstype::className(), ['id' => 'pollstypeid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userid']);
    }
    
    public static function getnumfyes($qtnid) {
        $firstyes=PollsUserAnswers::find()->select(['COUNT(*) AS count'])->where(['pollsid' => $qtnid])
        ->andWhere(['pollstypeid'=>'1'])
        ->andWhere(['numanswer'=>'1'])
        ->andWhere(['active'=>'1'])
        ->asArray()
        ->all();

        return $firstyes[0]['count'];
        
    }
    
    public static function getnumsyes($qtnid) {
        $firstyes=PollsUserAnswers::find()->select(['COUNT(*) AS count'])->where(['pollsid' => $qtnid])
        ->andWhere(['pollstypeid'=>'1'])
        ->andWhere(['numanswer'=>'2'])
        ->andWhere(['active'=>'1'])
        ->asArray()
        ->all();

        return $firstyes[0]['count'];
        
    }
    
    public static function getnumfno($qtnid) {
        $firstyes=PollsUserAnswers::find()->select(['COUNT(*) AS count'])->where(['pollsid' => $qtnid])
        ->andWhere(['pollstypeid'=>'2'])
        ->andWhere(['numanswer'=>'1'])
        ->andWhere(['active'=>'1'])
        ->asArray()
        ->all();

        return $firstyes[0]['count'];
        
    }
    
    public static function getnumsno($qtnid) {
        $firstyes=PollsUserAnswers::find()->select(['COUNT(*) AS count'])->where(['pollsid' => $qtnid])
        ->andWhere(['pollstypeid'=>'2'])
        ->andWhere(['numanswer'=>'2'])
        ->andWhere(['active'=>'1'])
        ->asArray()
        ->all();

        return $firstyes[0]['count'];
        
    }
    
    public static function getnumfagree($qtnid) {
        $firstyes=PollsUserAnswers::find()->select(['COUNT(*) AS count'])->where(['pollsid' => $qtnid])
        ->andWhere(['pollstypeid'=>'3'])
        ->andWhere(['numanswer'=>'1'])
        ->andWhere(['active'=>'1'])
        ->asArray()
        ->all();

        return $firstyes[0]['count'];
        
    }
    public static function getnumsagree($qtnid) {
        $firstyes=PollsUserAnswers::find()->select(['COUNT(*) AS count'])->where(['pollsid' => $qtnid])
        ->andWhere(['pollstypeid'=>'3'])
        ->andWhere(['numanswer'=>'2'])
        ->andWhere(['active'=>'1'])
        ->asArray()
        ->all();

        return $firstyes[0]['count'];
        
    }
    public static function getnumfdisagree($qtnid) {
        $firstyes=PollsUserAnswers::find()->select(['COUNT(*) AS count'])->where(['pollsid' => $qtnid])
        ->andWhere(['pollstypeid'=>'4'])
        ->andWhere(['numanswer'=>'1'])
        ->andWhere(['active'=>'1'])
        ->asArray()
        ->all();

        return $firstyes[0]['count'];
        
    }
    
    public static function getnumsdisagree($qtnid) {
        $firstyes=PollsUserAnswers::find()->select(['COUNT(*) AS count'])->where(['pollsid' => $qtnid])
        ->andWhere(['pollstypeid'=>'4'])
        ->andWhere(['numanswer'=>'2'])
        ->andWhere(['active'=>'1'])
        ->asArray()
        ->all();

        return $firstyes[0]['count'];
        
    }
}
