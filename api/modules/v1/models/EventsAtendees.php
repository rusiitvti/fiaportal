<?php

namespace app\api\modules\v1\models;

use Yii;
use yii\data\ActiveDataProvider;
use app\api\modules\v1\models\User;

/**
 * This is the model class for table "eventattendees".
 *
 * @property int $events_id
 * @property int $user_id
 * @property string $lastmodified
 * @property int $active
 *
 * @property Events $events
 * @property User $user
 */
class Eventsatendees extends \yii\db\ActiveRecord {


        public $event;


    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'eventattendees';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['events_id', 'user_id'], 'required'],
            [['events_id', 'user_id', 'active'], 'integer'],
            [['lastmodified'], 'safe'],
            [['events_id'], 'exist', 'skipOnError' => true, 'targetClass' => Events::className(), 'targetAttribute' => ['events_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'events_id' => 'Events ID',
            'user_id' => 'User ID',
            'lastmodified' => 'Lastmodified',
            'active' => 'Active',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvents() {
        return $this->hasOne(Events::className(), ['id' => 'events_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getUsers() {
//        $listCategory = User::find()->select('first_name,last_name')
//                ->where(['active' => '0'])
//                ->all();
        $query = User::find()->where(['active' => '0']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }

    public function getActiveUsers() {
        $query = User::find()->where(['active' => '1']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }

    public static function assignUsers($users) {
        $count = Yii::$app->db->createCommand()->batchInsert('eventattendees', ['events_id', 'user_id'], $users)->execute();
        if ($count > 0) {
            return 'Users Assigned Success!!';
            //return $this->redirect(['assign']);
        } else {
            return 'Error Assigned Users!!';
        }
    }

    public static function deactivateAttendees($eventsid, $userid) {
        Yii::$app->db->createCommand("UPDATE eventattendees SET active=0 WHERE events_id = '$eventsid' and user_id = '$userid' ")->execute();
        return 'Update Successfully';
    }

    public static function getusersnot($eventid) {
        $sbsquery = Eventsatendees::find()->select('user_id')->where(['events_id' => $eventid]);
        $model = User::find()
                ->where(['NOT IN', 'id', $sbsquery]);
        
        $dataProvider = new ActiveDataProvider([
            'query' => $model,
        ]);

        return $dataProvider;
//        $allusers = Yii::$app->db->createCommand("SELECT u.* FROM user u WHERE u.id NOT IN (SELECT e.user_id FROM eventattendees e where events_id = '$eventid')")->queryAll();
//        return $allusers;
    }

}
