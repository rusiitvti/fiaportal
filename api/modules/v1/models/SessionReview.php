<?php

namespace app\api\modules\v1\models;

use Yii;
use app\api\modules\v1\models\EventsAtendees;


/**
 * This is the model class for table "sessionreview".
 *
 * @property integer $reviews_id
 * @property integer $session_id
 *
 * @property Reviews $reviews
 * @property Session $session
 */
class SessionReview extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'sessionreview';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['reviews_id', 'session_id'], 'required'],
            [['reviews_id', 'session_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'reviews_id' => 'Reviews ID',
            'session_id' => 'Session ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReviews() {
        return $this->hasOne(Reviews::className(), ['id' => 'reviews_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSession() {
        return $this->hasOne(Session::className(), ['id' => 'session_id']);
    }

    public function getRatingsall($eventsponors) {


        //$alleventatt = Eventsatendees::find()->select('user_id')->where(['events_id' => $eventsponors]);

        $allsession = Session::find()
                ->andwhere(['hasdiscussion' => '1'])
                ->andWhere(['events_id' => $eventsponors])
                ->distinct()
                ->asArray()
                ->all();
        $temparr = [];
        $avg = 0;
        if (count($allsession) > 0) {
            foreach ($allsession as $asession) {
                $speaker = [];

                $allreviews = SessionReview::find()
                                ->select(['reviews_id'])
                                ->where(['session_id' => $asession['id']])
                                ->asArray()->all();
                if (count($allreviews) > 0) {
                    $count = count($allreviews);
                    $total = 0;
                    foreach ($allreviews as $reviewid) {
                        $ratins = Reviews::find()->select(['ratings'])->where(['id' => $reviewid])->one();
                        $intval = intval($ratins['ratings']);
                        $total += $intval;
                    }
                    $tempavg = $total / $count;
                    //$avg = number_format((float) $tempavg, 2, '.', '');
                    $avg = round($tempavg);
                }
                $speaker['name'] = $asession['program'];
                $speaker['avg'] = $avg;
                array_push($temparr, $speaker);
            }
        }
        return $temparr;
    }

}
