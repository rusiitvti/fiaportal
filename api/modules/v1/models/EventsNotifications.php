<?php

namespace app\api\modules\v1\models;

use Yii;

/**
 * This is the model class for table "eventsnotification".
 *
 * @property integer $events_id
 * @property integer $notification_id
 *
 * @property Events $events
 * @property Notification $notification
 */
class EventsNotifications extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'eventsnotification';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['events_id', 'notification_id'], 'required'],
            [['events_id', 'notification_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'events_id' => 'Events ID',
            'notification_id' => 'Notification ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvents()
    {
        return $this->hasOne(Events::className(), ['id' => 'events_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotification()
    {
        return $this->hasOne(Notification::className(), ['id' => 'notification_id']);
    }
}