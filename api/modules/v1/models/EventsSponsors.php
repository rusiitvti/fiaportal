<?php
namespace app\api\modules\v1\models;

use Yii;
use yii\data\ActiveDataProvider;
use app\api\modules\v1\models\Sponsors;
/**
 * This is the model class for table "eventssponsors".
 *
 * @property integer $events_id
 * @property integer $sponsors_id
 *
 * @property Events $events
 * @property Sponsors $sponsors
 */
class EventsSponsors extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'eventssponsors';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['events_id', 'sponsors_id'], 'required'],
            [['events_id', 'sponsors_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'events_id' => 'Events ID',
            'sponsors_id' => 'Sponsors ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvents()
    {
        return $this->hasOne(Events::className(), ['id' => 'events_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSponsors()
    {
        return $this->hasOne(Sponsors::className(), ['id' => 'sponsors_id']);
    }
    
    public function getSponsorsAll() {
        $query = Sponsors::find()->where(['active' => '1']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        return $dataProvider;
    }
    
      public static function assignSponsors($sponsors) {
        $count = Yii::$app->db->createCommand()->batchInsert('eventssponsors', ['events_id', 'sponsors_id'], $sponsors)->execute();
        if ($count > 0) {
            return 'Sponsor Assigned Success!!';
        } else {
            return 'Error Assigned Sponsors!!';
        }
    }

    public static function getsponsorsnot($eventid) {
        $sbsquery = eventssponsors::find()->select('id')->where(['events_id' => $eventid]);//select all from events where event id = $eventid
        
        $model = Sponsors::find()
                ->where(['NOT IN', 'id', $sbsquery]);
        
        $dataProvider = new ActiveDataProvider([
            'query' => $model,
        ]);

        return $dataProvider;
//        $allusers = Yii::$app->db->createCommand("SELECT u.* FROM user u WHERE u.id NOT IN (SELECT e.user_id FROM eventattendees e where events_id = '$eventid')")->queryAll();
//        return $allusers;
    }



}