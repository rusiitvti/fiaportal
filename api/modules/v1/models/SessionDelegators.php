<?php

namespace app\api\modules\v1\models;

use Yii;

/**
 * This is the model class for table "sessiondelegators".
 *
 * @property int $session_id
 * @property int $user_id
 * @property string $type
 * @property string $lastmodified
 *
 * @property Session $session
 * @property User $user
 */
class SessionDelegators extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sessiondelegators';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['session_id', 'user_id'], 'required'],
            [['session_id', 'user_id'], 'integer'],
            [['lastmodified'], 'safe'],
            [['type'], 'string', 'max' => 100],
            [['session_id'], 'exist', 'skipOnError' => true, 'targetClass' => Session::className(), 'targetAttribute' => ['session_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'session_id' => 'Session ID',
            'user_id' => 'User ID',
            'type' => 'Type',
            'lastmodified' => 'Lastmodified',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSession()
    {
        return $this->hasOne(Session::className(), ['id' => 'session_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
   
}
