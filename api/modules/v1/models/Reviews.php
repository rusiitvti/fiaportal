<?php

namespace app\api\modules\v1\models;

use Yii;

/**
 * This is the model class for table "reviews".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $ratings
 * @property string $message
 * @property integer $active
 * @property string $lastmodified
 *
 * @property Eventreviews[] $eventreviews
 * @property User $user
 * @property Sessionreview[] $sessionreviews
 * @property Speakersreview[] $speakersreviews
 */
class Reviews extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'reviews';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'user_id'], 'required'],
            [['id', 'user_id', 'active'], 'integer'],
            [['lastmodified'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'ratings' => 'Ratings',
            'message' => 'Message',
            'active' => 'Active',
            'lastmodified' => 'Lastmodified',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventreviews() {
        return $this->hasMany(Eventreviews::className(), ['reviews_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSessionreviews() {
        return $this->hasMany(Sessionreview::className(), ['reviews_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpeakersreviews() {
        return $this->hasMany(Speakersreview::className(), ['reviews_id' => 'id']);
    }

    public static function addReviews($type, $id, $userid, $rate, $message) {

        $status = '';
        if ($type == 'session') {
            $model = new Reviews();
            $model->user_id = $userid;
            $model->ratings = $rate;
            $model->message = $message;
            $model->save(false);

            $sessionreviews = new SessionReview();
            $sessionreviews->reviews_id = $model->id;
            $sessionreviews->session_id = $id;
            $sessionreviews->save(false);
            $status .= 'Session Review Added!!';
            //$model->link('sessionreviews', $sessionreviews);
        } else if ($type == 'speaker') {
            $model = new Reviews();
            $model->user_id = $userid;
            $model->ratings = $rate;
            $model->message = $message;
            $model->save(false);

            $speakereview = new SpeakersReview();
            $speakereview->reviews_id = $model->id;
            $speakereview->speaker_id = $id;
            $speakereview->save(false);
            $status .= 'Speaker Review Added!!';
        } else {
            $model = new Reviews();
            $model->user_id = $userid;
            $model->ratings = $rate;
            $model->message = $message;
            $model->save(false);

            $eventreview = new EventsReviews();
            $eventreview->reviews_id = $model->id;
            $eventreview->events_id = $id;
            $eventreview->save(false);
            $status .= 'Event Review Added!!';
        }
        return $status;
    }

}
