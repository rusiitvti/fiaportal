<?php

namespace app\api\modules\v1\models;

use Yii;


use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use app\api\modules\v1\models\Query;
use app\api\modules\v1\models\PollsUserAnswers;
use app\api\modules\v1\models\PollsQuestionAnswer;

/**
 * This is the model class for table "polls".
 *
 * @property int $id
 * @property string $question
 * @property string $lastmodified
 * @property int $active
 * @property int $session
 *
 * @property Session $session0
 * @property PollsquestionAnswer[] $pollsquestionAnswers
 * @property Pollstype[] $pollsanswers
 * @property PollsuserAnswers[] $pollsuserAnswers
 */
class Polls extends \yii\db\ActiveRecord
{
    
    public $answer = [];
    public $userans;
    public $quest;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'polls';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['question','answer'], 'required'],
            [['question'], 'string'],
            [['lastmodified'], 'safe'],
            [['active', 'session'], 'integer'],
            [['session'], 'exist', 'skipOnError' => true, 'targetClass' => Session::className(), 'targetAttribute' => ['session' => 'id']],
            [['event_id'], 'exist', 'skipOnError' => true, 'targetClass' => Events::className(), 'targetAttribute' => ['event_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'question' => 'Question',
            'lastmodified' => 'Lastmodified',
            'active' => 'Active',
            'session' => 'Session',
            'userans' => ' ',
            'quest' => ' ',
            'event_id' => 'Event',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSession0()
    {
        return $this->hasOne(Session::className(), ['id' => 'session']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvents0()
    {
        return $this->hasOne(Events::className(), ['id' => 'event_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPollsquestionAnswers()
    {
        return $this->hasMany(PollsquestionAnswer::className(), ['pollsquestionid' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPollsanswers()
    {
        return $this->hasMany(Pollstype::className(), ['id' => 'pollsanswerid'])->viaTable('pollsquestion_answer', ['pollsquestionid' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPollsuserAnswers()
    {
        return $this->hasMany(PollsuserAnswers::className(), ['pollsid' => 'id']);
    }
    
    public function getAnswerDropdown() {
        $listanswers = PollsType::find()->select('id,answer')
                ->all();
        $list = ArrayHelper::map($listanswers, 'id', 'answer');


        return $list;
    }

    public function getQuestionans($id) {

        $query = new \yii\db\Query;
        $query->select([
                    'pollstype.answer',
                    'pollsquestion_answer.pollsanswerid'
                        ]
                )
                ->from('pollsquestion_answer')
                ->join('INNER JOIN', 'pollstype', 'pollstype.id=pollsquestion_answer.pollsanswerid')
                ->WHERE(['pollsquestion_answer.pollsquestionid' => $id]);

        $command = $query->createCommand();
        $data = $command->queryAll();

        $list = ArrayHelper::map($data, 'pollsanswerid', 'answer');

        return $list;
    }

    public function getActQtn() {
        $listanswers = Polls::find()->where(['active' => '1'])->asArray()->all();

        $finalQuestionArr = [];
        if (count($listanswers) > 0) {
            foreach ($listanswers as $aquestion) {
                $temp = [];
                $question = strip_tags($aquestion['question']);
                $temp['id'] = $aquestion['id'];
                $temp['question'] = $question;
                array_push($finalQuestionArr, $temp);
            }
            $list = ArrayHelper::map($finalQuestionArr, 'id', 'question');
            return $list;
        }
    }
    
    public static function getcountuserans($userid, $qtnid) {
        $firstyes=PollsUserAnswers::find()->select(['COUNT(*) AS count'])->where(['userid' => $userid])->andWhere(['pollsid'=>$qtnid])->asArray()->all();

        return $firstyes[0]['count'];
        
    }
}