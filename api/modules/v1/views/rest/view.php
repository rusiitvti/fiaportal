<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\api\modules\v1\models\PanelDiscussion */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Panel Discussions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="well">

    <h1>Question Submitted Successfully!!</h1>
    <br/>
    
    <a href="<?= Yii::$app->params['server'] . Yii::$app->params['servername'] ?>rest/panels?sessionid=<?= $session ?>" class="btn btn-success">Ask another Question</a>

</div>
