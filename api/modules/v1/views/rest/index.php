<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'List of Questions';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php
$script = <<< JS
$(document).ready(function() {
         setTimeout(function() {
       window.location.href = window.location;
    }, 10000);
    
});
JS;
$this->registerJs($script);
?>
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

    </div>
    <div class="box-body">


        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [

                [
                    'attribute' => 'session_id',
                    'value' => function ($model) {
                        //array_push($model->totalDays, totalDays, $model->class->date);
                        return $model->getSessions($model->session_id);
                    },
                ],
                'question',
                [
                    'attribute' => 'user_id',
                    'value' => function ($model) {
                        //array_push($model->totalDays, totalDays, $model->class->date);
                        return $model->getSpeakersname($model->user_id);
                    },
                ],
                ['class' => 'yii\grid\ActionColumn', 'template' => '{delete}'],
            ],
        ]);
        ?>
    </div>
</div>
