<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\api\modules\v1\models\PanelDiscussion */

$this->title = 'Update Panel Discussion: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Panel Discussions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="panel-discussion-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
