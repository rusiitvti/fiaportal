<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\api\modules\v1\models\Session;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\api\modules\v1\models\PanelDiscussion */
/* @var $form yii\widgets\ActiveForm */
$session = new Session();
?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Ask a Question</h3>
    </div>
        <?php $form = ActiveForm::begin(); ?>
    <div class="box-body">
        <?=
        $form->field($model, 'session_id')->dropDownList(
                ArrayHelper::map(Session::getSessionpanel($sessionid), 'id', 'program')
        )
        ?>

<?= $form->field($model, 'question')->textarea(['maxlength' => true]) ?>



<?= 
                $form->field($model, 'user_id')->dropDownList(
                $session->getSpeakerpanel($sessionid), [
            'prompt' => 'Select Panels',
                ]
        )?>

        <input type="hidden" value="<?= $sessionid?>" name="sessionid" />
        <div class="form-group">
<?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>

<?php ActiveForm::end(); ?>
    </div>
</div>
