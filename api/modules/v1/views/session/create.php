<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\api\modules\v1\models\PanelDiscussion */

$this->title = 'Create Panel Discussion';
$this->params['breadcrumbs'][] = ['label' => 'Panel Discussions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel-discussion-create">

  

    <?= $this->render('_form', [
        'model' => $model,'sessionid' => $sessionid
    ]) ?>

</div>
