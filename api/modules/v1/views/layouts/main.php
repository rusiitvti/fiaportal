<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <?php $this->beginBody() ?>

        <div class="wrapper">
            <header class="main-header">
                <!-- Logo -->
                <a href="#" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><b>F</b>IA</span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b>FIA Portal</b></span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>

                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <!-- Messages: style can be found in dropdown.less-->
                            <li class="dropdown messages-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-arrow-circle-down"></i>
                                    <span class="label label-success">Logout</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="header">Sign Out of FIA Portal</li>
                                    <li>
                                        <!-- inner menu: contains the actual data -->
                                        <ul class="menu">
                                            <li>
                                                <a href="<?= Url::toRoute(['site/destroylogin']) ?>">
                                                    <i class="fa fa-power-off text-aqua"></i> Logout
                                                </a>
                                            </li>

                                        </ul>
                                    </li>
                                    <li class="footer"><a href="#">Hope to see you again !!</a></li>
                                </ul>
                            </li>
                            <!-- Notifications: style can be found in dropdown.less -->
                            <!--                            <li class="dropdown notifications-menu">
                                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                                <i class="fa fa-bell-o"></i>
                                                                <span class="label label-warning">10</span>
                                                            </a>
                                                            <ul class="dropdown-menu">
                            
                                                            </ul>
                                                        </li>
                                                         Tasks: style can be found in dropdown.less 
                                                        <li class="dropdown tasks-menu">
                                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                                <i class="fa fa-flag-o"></i>
                                                                <span class="label label-danger">9</span>
                                                            </a>
                                                            <ul class="dropdown-menu">
                                                                <li class="header">You have 9 tasks</li>
                            
                                                            </ul>
                                                        </li>-->
                            <!-- User Account: style can be found in dropdown.less -->

                            <!-- Control Sidebar Toggle Button -->

                        </ul>
                    </div>
                </nav>
            </header>
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->


                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu" data-widget="tree">
                        <li class="header"> <a href="<?= Url::toRoute(['site/dashboard']) ?>">FIA Dashboard</a></li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-balance-scale"  style="color:#F0FB05"></i> <span>Events Management</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">

                                <li><a href="<?= Url::toRoute(['event/index']) ?>"><i class="fa fa-circle-o"></i> Event List</a></li>
                                <li><a href="<?= Url::toRoute(['event/create']) ?>"><i class="fa fa-circle-o"></i> Create a FIA Event</a></li>
                                <li><a href="<?= Url::toRoute(['event/assign']) ?>"><i class="fa fa-circle-o"></i> Assign Users to an Event</a></li>
                                <li><a href="<?= Url::toRoute(['event/assignsponsor']) ?>"><i class="fa fa-circle-o"></i> Assign Sponsors to an Event</a></li>

                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-calendar-check-o" style="color:#4467F7"></i> <span>Sessions Management</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li> <a href="<?= Url::toRoute(['session/index']) ?>"><i class="fa fa-circle-o"></i> Program List</a></li>
                                <li><a href="<?= Url::toRoute(['session/create']) ?>"><i class="fa fa-circle-o"></i> Create a Session</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-user"  style="color:#F90202"></i> <span>User Management</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li> <a href="<?= Url::toRoute(['user/index']) ?>"><i class="fa fa-circle-o"></i> User List</a></li>
                                <li><a href="<?= Url::toRoute(['user/create']) ?>"><i class="fa fa-circle-o"></i> Create a User</a></li>
                                <li><a href="<?= Url::toRoute(['user/activateusers']) ?>"><i class="fa fa-circle-o"></i> Activate Users</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-gift" style="color:#4467F7"></i> <span>Sponsor Management</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li> <a href="<?= Url::toRoute(['sponsor/index']) ?>"><i class="fa fa-circle-o"></i> Sponsor List</a></li>
                                <li><a href="<?= Url::toRoute(['sponsor/create']) ?>"><i class="fa fa-circle-o"></i> Create a Sponsor</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-map-marker" style="color:#F90202"></i> <span>Venue Management</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li> <a href="<?= Url::toRoute(['venue/create']) ?>"><i class="fa fa-circle-o"></i> Create a Venue</a></li>
                                <li><a href="<?= Url::toRoute(['venue/index']) ?>"><i class="fa fa-circle-o"></i> List Of Venues</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-question-circle-o" style="color:#4467F7"></i> <span>Polls Management</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li> <a href="<?= Url::toRoute(['poll/create']) ?>"><i class="fa fa-circle-o"></i> Create a Question</a></li>
                                <li><a href="<?= Url::toRoute(['poll/index']) ?>"><i class="fa fa-circle-o"></i> List Of Questions</a></li>
                                <li><a href="<?= Url::toRoute(['poll/askquestion']) ?>"><i class="fa fa-circle-o"></i> Ask Question</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-commenting" style="color:#F0FB05"></i> <span>Others</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                               
                                 <li> <a href="<?= Url::toRoute(['panel/index']) ?>"><i class="fa fa-circle-o"></i> List of Questions</a></li>
                                <li> <a href="#"><i class="fa fa-circle-o"></i> Push Notification</a></li>
                            </ul>
                        </li>
                        <!--                        <li class="treeview">
                                                    <a href="#">
                                                        <i class="fa fa-bell-o" style="color:#17C5F5"></i> <span>Push Notification</span>
                                                        <span class="pull-right-container">
                                                            <i class="fa fa-angle-left pull-right"></i>
                                                        </span>
                                                    </a>
                                                    <ul class="treeview-menu">
                                                        <li> <a href="#"><i class="fa fa-circle-o"></i> Send a Notification</a></li>
                                                        <li><a href="<?= Url::toRoute(['user/create']) ?>"><i class="fa fa-circle-o"></i> Create a User</a></li>
                                                    </ul>
                                                </li>
                        -->
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>


            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->


                <!-- Main content -->
                <section class="content">
                    <?= $content ?>
                </section>
            </div>
        </div>

        <footer class="footer">
            <div class="container">
                <p class="pull-left">&copy; Fiji Institute of Accountants <?= date('Y') ?></p>

                <p class="pull-right">Powered by <a href="http://www.itvti.com.fj" target="_blank">iTvTi</a></p>
            </div>
        </footer>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
