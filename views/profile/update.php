<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\api\modules\v1\models\PanelDiscussion */

$this->title = 'Update Profile';
$this->params['breadcrumbs'][] = ['label' => 'Panel Discussions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="panel-discussion-update">



    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
