<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\api\modules\v1\models\Events;
use app\api\modules\v1\models\UserType;

/* @var $this yii\web\View */
/* @var $model app\api\modules\v1\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">
            <?php if ($model->isNewRecord) { ?> Create User <?php } else { ?>
                Update Profile
            <?php } ?>
        </h3>
    </div>

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="box-body">
        <?php
        if ($model->isNewRecord) {
            echo $form->field($model, 'file')->fileInput();
        } else {
            echo Html::img(Yii::$app->params['server'] . $model->picture, ['width' => '200px', 'height' => '200px']);
            echo $form->field($model, 'file')->fileInput();
        }
        ?>
        <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'oldpassword')->passwordInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'newpassword')->passwordInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'confirmnewpass')->passwordInput(['maxlength' => true]) ?>
        <div class="box-footer">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
