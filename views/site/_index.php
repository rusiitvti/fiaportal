<?php
$events = new app\api\modules\v1\models\Events();
$user = new app\api\modules\v1\models\User();
$eventreviews = new app\api\modules\v1\models\EventsReviews();
$speakerreviews = new app\api\modules\v1\models\SpeakersReview();
$sessionreview = new app\api\modules\v1\models\SessionReview();




if (!empty($_GET)){
    $eventid = $_GET['eventid'];
    $model = Events::findOne($eventid);
}

$types = "speaker";

//echo $eventid;
?>



<div class="box box-warning">
        <div class="box-header with-border">
            <h3 class="box-title headline">RATINGS</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body" style="">
            <div class="col-lg-6 col-xs-12">
                <header class="headline">
                    <h4 class="checked">Speaker Ratings</h4>
                </header>
                <table class="table table-bordered">
                    <thead>
                    <th>Speaker Name</th>
                    <th>Image</th>
<!--                        <th>Average Ratings</th>-->
                    <th>Ratings</th>
                    </thead>
                    <tbody>
                        <?php foreach ($speakerreviews->getRatingsall($eventsponors) as $areview) { ?>
                            <tr>
                                <td>
                                    <p> <?= $areview['pic'] ?> </p
                                </td>
                                <td>
                                    <img class="img-rounded" src="<?= Yii::$app->params['server'] . $areview['name'] ?>" style="width:100px !important;height:100px !important" />
                                </td>
                                <td>
                                    <?php
                                    for ($i = 1; $i <= $areview['avg']; $i++) {
                                        ?>
                                        <span class="fa fa-star checked"></span>
                                    <?php } ?>
                                    <?php for ($i = $areview['avg']; $i < 5; $i++) {
                                        ?>
                                        <span class="fa fa-star"></span>
                                    <?php } ?>
                                </td>
                            </tr>

                        <?php }
                        ?>
                    </tbody>
                </table>
            </div>
            <div class="col-lg-6 col-xs-12">
                <header class="headline">
                    <h4 class="checked">Session Ratings</h4>
                </header>
                <table class="table table-bordered">
                    <thead>
                    <th>Session Name</th>
<!--                        <th>Average Ratings</th>-->
                    <th>Ratings</th>
                    </thead>

                    <tbody>
                        <?php foreach ($sessionreview->getRatingsall($eventsponors) as $areview) { ?>

                            <tr>
                                <td><?= $areview['name'] ?>

                                </td>
                                <td>
                                    <?php
                                    for ($i = 1; $i <= $areview['avg']; $i++) {
                                        ?>
                                        <span class="fa fa-star checked"></span>
                                    <?php } ?>
                                    <?php for ($i = $areview['avg']; $i < 5; $i++) {
                                        ?>
                                        <span class="fa fa-star"></span>
                                    <?php } ?>
                                </td>
                            </tr>

                        <?php }
                        ?>
                    </tbody>

                </table>
            </div>

        </div>
        <!-- /.box-body -->
    </div>


