<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="login-box">
    <div class="login-logo">
        <?php echo Html::img(\Yii::$app->homeUrl . '/images/logo2.png', ['width' => '200', 'height' => '200'], ['alt' => 'imagelogo']); ?>
    </div>
   <div class="login-box-body">
    <p class="login-box-msg">Welcome to the FIA Portal</p>
        <p>Please fill out the following fields to login:</p>

        <?php
        $form = ActiveForm::begin(['id' => 'login-form']);
        ?>

        <?= $form->field($model, 'username',['options' => [
            'tag' => 'div',
            'class' => 'form-group field-loginform-username has-feedback required'
        ],
            'template' => '{input}<span class="fa fa-user form-control-feedback" style="color:grey"></span>{error}{hint}'
            
        ])->textInput(['placeholder' => 'Username']) ?>

        <?= $form->field($model, 'password',['options' => [
            'tag' => 'div',
            'class' => 'form-group field-loginform-password has-feedback required'
        ],
            'template' => '{input}<span class="fa fa-lock form-control-feedback" style="color:grey"></span>{error}{hint}'
            
        ])->passwordInput() ?>
        
        <div class="form-group">
                <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
    <div class="col-lg-offset-1" style="color:#999;">

    </div>

</div>
