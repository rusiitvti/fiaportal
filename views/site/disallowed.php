<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;


?>
<div class="well">

   
    <h1>Unauthorized Access</h1>
    <p>
       You are not allowed to access the FIA PORTAL
    </p>
    <p>
        Please contact us if you think this is a server error. Thank you.
    </p>
    <a href="<?= Yii::$app->params['server'] . Yii::$app->params['servername'] ?>/site/logout" class="btn btn-success">BACK</a>
</div>
