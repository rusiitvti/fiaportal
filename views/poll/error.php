
<div class="alert alert-success alert-dismissable">
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button" style="color:white;font-weight: bolder">×</button>
    <h1><?= $error ?></h1>

</div>

