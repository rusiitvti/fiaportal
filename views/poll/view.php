<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\api\modules\v1\models\Polls */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Polls', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="polls-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            
            'question:html',
//            'lastmodified',
//            'active',
            'session0.program',

            'events0.name',
        ],
//        [
//                'attribute' => 'question',
//                 'value' => function ($model) {
//                                //array_push($model->totalDays, totalDays, $model->class->date);
//                                return $model->question;
//                            },
//                'format' => ['html'],
//            ],
    ]) ?>

</div>