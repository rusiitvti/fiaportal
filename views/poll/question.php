<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\db\ActiveQuery;
use app\api\modules\v1\models\Polls;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model app\api\modules\v1\models\Events */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Answer Question';

$pollquestions = new Polls();

$polltype = new app\api\modules\v1\models\PollsType();

$usercount = $pollquestions->getcountuserans($userid, $model->id);

//echo $usercount;

if ($usercount =='0'){

//$id =$_POST['Polls']['question'];
?>
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Question</h3>
    </div>
<?php $form = ActiveForm::begin(['action' => ['poll/questionanswered'], 'method' => 'post',]); ?>

    <div class="box-body">
        <div style="font-size: 15px; margin-left: 20px;">
            <?= Html::getAttributeValue($model, 'question') ?>

            <?= $form->field($model, 'userans')->radioList($pollquestions->getQuestionans($model->id)) ?>
        </div>

        <div class="box-footer">
            <div class="row pull-right">
                <input type="submit" class="btn btn-success"  value="Submit" />
                <input type="hidden" value="<?= $model->id ?>" name="id"/>
                <input type="hidden" value="<?= $userid ?>" name="userid"/>
            </div>

        </div>
<?php ActiveForm::end(); ?>
    </div>
</div>

<?php }

else if ($usercount=='1'){
    
?>
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Answer Again</h3>
    </div>
<?php $form = ActiveForm::begin(['action' => ['poll/questionanswered'], 'method' => 'post',]); ?>

    <div class="box-body">
        <div style="font-size: 15px; margin-left: 20px;">
            <?= Html::getAttributeValue($model, 'question') ?>

            <?= $form->field($model, 'userans')->radioList($pollquestions->getQuestionans($model->id)) ?>
        </div>

        <div class="box-footer">
            <div class="row pull-right">
                <input type="submit" class="btn btn-success"  value="Submit" />
                <input type="hidden" value="<?= $model->id ?>" name="id"/>
                <input type="hidden" value="<?= $userid ?>" name="userid"/>
            </div>

        </div>
<?php ActiveForm::end(); ?>
    </div>
</div>
<?php }

else{
?>
    <div class="alert alert-success alert-dismissable">
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button" style="color:white;font-weight: bolder">×</button>
    <h4>You have exceeded your limit of answering the question.
       Thank you for participating !!</h4>

    </div>
<?php
}
?>

