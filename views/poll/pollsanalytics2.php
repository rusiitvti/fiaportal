<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;
use app\api\modules\v1\models\Polls;


$qtnid= $_GET['qtnid'];

$pollanswers = new app\api\modules\v1\models\PollsUserAnswers;
$this->title = 'Polls Analytics';

$pollquestions = new app\api\modules\v1\models\Polls();

$model =Polls::findOne($qtnid);

$polltype = new app\api\modules\v1\models\PollsType();


$fyes= $pollanswers->getnumfyes($qtnid);
$syes= $pollanswers->getnumsyes($qtnid);
$fno= $pollanswers->getnumfno($qtnid);
$sno= $pollanswers->getnumsno($qtnid);
$fagree= $pollanswers->getnumfagree($qtnid);
$sagree= $pollanswers->getnumsagree($qtnid);
$fdisagree= $pollanswers->getnumfdisagree($qtnid);
$sdisagree= $pollanswers->getnumsdisagree($qtnid);



$val1=0;
$val2 =0;
$val3 = 0;
$val4=0;

$name1 =" ";
$name2 =" ";

if($fyes>0){
    $name1 ="Yes";
    $value1 = $fyes;
    $val1 = $value1 *1;
}

if($fno>0){
    $name2 ="No";
    $value2 = $fno;
    
    $val2 = $value2*1;
}

if($fagree>0){
    $name1 ="Agree";
    $value1 = $fagree;
    $val1 = $value1 *1;
}

if($fdisagree>0){
    $name2 ="Disagree";
    $value2 = $fdisagree;
    
    $val2 = $value2*1;
}

if($syes>0){
    $value3 = $syes;
    $val3 = $value3 * 1;
}

if($sno>0){
    $value4 = $sno;
    $val4 = $value4 *1;
}

if($sagree>0){
    $value3 = $sagree;
    $val3 = $value3 * 1;
}

if($sdisagree>0){
    $value4 = $sdisagree;
    $val4 = $value4 *1;
}

?>

<div class="box box-primary">
    <div class="box-header with-border">
    </div>
    
    <div style="font-size: 25px; margin-left: 20px; margin-bottom:30px">
        <?= Html::getAttributeValue($model, 'question') ?>
    </div>
    
    <?php echo Highcharts::widget([
    'scripts' => [
        'modules/exporting',
        'themes/grid-light',
    ],
    'options' => [
        'title' => [
            'text' => 'Poll Response',
        ],
        
        'labels' => [
            'items' => [
                [
                    'html' => 'Pre-Poll',
                    'style' => [
                        'left' => '320px',
                        'top' => '10px',
                        'color' => new JsExpression('(Highcharts.theme && Highcharts.theme.textColor) || "black"'),
                        
                    ],
                ],
                
                [
                    'html' => 'Final Poll',
                    'style' => [
                        'left' => '780px',
                        'top' => '10px',
                        'color' => new JsExpression('(Highcharts.theme && Highcharts.theme.textColor) || "black"'),
                    ],
                ],
            ],
        ],
        'series' => [
            
            [
                'type' => 'pie',
                'name'=>'Pre-Poll',
                'data' => [
                    [
                        'name' => $name1,
                        'y' => $val1 ,
                        'color' => new JsExpression('Highcharts.getOptions().colors[0]'), // Jane's color
                    ],
                    [
                        'name' => $name2,
                        'y' => $val2,
                        'color' => new JsExpression('Highcharts.getOptions().colors[1]'), // John's color
                    ],
                    
                    
                ],
                'center' => [350, 150],
                'size' => 150,
                'showInLegend' => true,
                'dataLabels' => [
                    'enabled' => true,
                    'format'=> '<b>{point.name}</b>: {point.percentage:.1f} %',
                ],
            ],
            
            [
                'type' => 'pie',
                'name'=> 'Final Poll',
                'data' => [
                    [
                        'name' => $name1,
                        'y' => $val3,
                        'color' => new JsExpression('Highcharts.getOptions().colors[0]'), // Jane's color
                    ],
                    [
                        'name' => $name2,
                        'y' => $val4,
                        'color' => new JsExpression('Highcharts.getOptions().colors[1]'), // John's color
                    ],
                    
                ],
                'center' => [750, 150],
                'size' => 150,
                'showInLegend' => false,
                'dataLabels' => [
                    'enabled' => true,
                    'format'=> '<b>{point.name}</b>: {point.percentage:.1f} %',
                ],
            ],
        ],
    ]
]); ?>

</div>

