<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use dosamigos\ckeditor\CKEditor;
use kartik\select2\Select2;
use app\api\modules\v1\models\Session;
use app\api\modules\v1\models\PollsQuestionAnswer;
use app\api\modules\v1\models\Events;
/* @var $this yii\web\View */
/* @var $model app\api\modules\v1\models\Polls */
/* @var $form yii\widgets\ActiveForm */

$events = new app\api\modules\v1\models\Events();
?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">
           Create Polls
        </h3>
    </div>
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="box-body">
         <?= $form->field($model, 'event_id')->dropDownList(
                ArrayHelper::map(Events::find()->all(), 'id', 'name'), ['prompt' => 'Select Events']
    )
    ?>

    <?= $form->field($model, 'session')->dropDownList(
            ArrayHelper::map(Session::find()->where(['active' => '1'])->andWhere(['isdebate' => '1'])->asArray()->all(), 'id', 'program'), ['prompt' => 'Select Session']
    )
    ?>

    <?= $form->field($model, 'question')->widget(CKEditor::className(), [
        'options' => ['rows' => 4],
        'preset' => 'basic'
    ]) ?>
    
    <?php
        if (!$model->isNewRecord) {
            $anstype = $model->pollsquestionAnswers;
            $selecteds = [];
            foreach ($anstype as $atype) {
                $id = $atype->pollsanswerid;
                $selecteds[] = $id;
            }
         
          
            $model->answer = $selecteds;
           // echo 'update';
            
         echo  $form->field($model, 'answer')->widget(Select2::classname(), [
                'data' => $model->AnswerDropdown,
                'options' => ['multiple' => true],
            ]);
         
        } else {
            // echo 'new record';
//          
            echo  $form->field($model, 'answer')->widget(Select2::classname(), [
                'data' => $model->AnswerDropdown,
                'language' => 'de',
                'options' => ['multiple' => true, 'placeholder' => 'Select answers ...'],
                'pluginOptions' => [
                'allowClear' => true
                ],
            ]);
        }
    ?>
    

    <div class="box-footer">
    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

    <?php ActiveForm::end(); ?>
    </div>
</div>