<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Polls';
$this->params['breadcrumbs'][] = $this->title;

$gtq = "poll/question";

 ?>

<div class="polls-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Polls', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

             [
                'attribute' => 'question',
                 'value' => function ($model) {
                                //array_push($model->totalDays, totalDays, $model->class->date);
                                return $model->question;
                            },
                'format' => ['html'],
            ],
                                    
            [
                'attribute' => 'session',
                'value' => 'session0.program'
            ],

            [
                'attribute' => 'event_id',
                'value' => 'events0.name'
            ],
             
//            [
//                'attribute' => 'quest',
//                'format' => 'raw',
//                'value' => function ($model) { 
//                        return Html::a('User View - Question', [ 'poll/question', 'session' => $model->session ], ['target' => '_blank']);
//                    //return Html::url('poll/question');;
//                },
//            ],
            

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>