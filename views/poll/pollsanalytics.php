<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;

$this->title = 'Choose Question';

$pollquestions = new app\api\modules\v1\models\Polls();

$polltype = new app\api\modules\v1\models\PollsType();

//$yes=10;

//echo $yes;
//echo $qtnid;

?>
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Choose Question</h3>
    </div>
    <?php $form = ActiveForm::begin(['action' => ['poll/question'], 'method' => 'post']); ?>

    <div class="box-body">

        <?=
        
        $form->field($pollquestions, 'question')->dropDownList($pollquestions->ActQtn , [ 'prompt' => 'Select Question']
        )
        ?>
        <br>
        
    </div>
    

    <?php ActiveForm::end(); ?>
    
   

</div>
