<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\api\modules\v1\models\Events;
use app\api\modules\v1\models\UserType;

/* @var $this yii\web\View */
/* @var $model app\api\modules\v1\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">
            <?php if ($model->isNewRecord) { ?> Create User <?php } else { ?>
            Update User
            <?php } ?>
        </h3>
    </div>

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="box-body">

        <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'company_name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'designation')->textInput(['maxlength' => true]) ?>

        <?php if($model->isNewRecord){
            echo $form->field($model, 'file')->fileInput();
        }else{
             echo Html::img(Yii::$app->params['server'] . $model->picture, ['width' => '150px']);
             echo $form->field($model, 'file')->fileInput();
        }
        ?>
      
        <?php
        if (!$model->isNewRecord) {
            $usertypes = $model->userhastypes;
            $selecteds = [];
            foreach ($usertypes as $atype) {
                $id = $atype->userType_id;
                $selecteds[] = $id;
            }
         
          
            $model->usertype = $selecteds;
           // echo 'update';
            // $model->usertype
            echo $form->field($model, 'usertype')->checkboxList($model->UserDropdown)->label(FALSE);
        } else {
            // echo 'new record';
          echo  $form->field($model, 'usertype')->checkboxList(
                    $model->UserDropdown, [
                    ]
            );
        }
        ?>

        <?=
        $form->field($model, 'gender')->dropDownList(
                array('Male' => 'Male', 'Female' => 'Female'), ['prompt' => 'Select Gender']
        )
        ?>
        <?= $form->field($model, 'qualification')->textInput(['maxlength' => true]) ?>




        <div class="box-footer">
<?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>
            <?php ActiveForm::end(); ?>

</div>
