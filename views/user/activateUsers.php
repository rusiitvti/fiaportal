
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\api\modules\v1\models\Events;
use app\api\modules\v1\models\User;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\api\modules\v1\models\Events */
/* @var $form yii\widgets\ActiveForm */
$eventAtendees = new app\api\modules\v1\models\EventsAtendees();
$this->title = 'Activate Users';
?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Activate Users</h3>
    </div>
    <?php $form = ActiveForm::begin(['action' => ['user/activate'], 'method' => 'post',]); ?>

    <div class="box-body">


        <h4>Inactive Users</h4>
        <br>
        <?=
        GridView::widget([
            'dataProvider' => $eventAtendees->users,
            'id' => 'usergrid',
            'columns' => [
                [
                    'attribute' => 'picture',
                    'format' => 'html',
                    'value' => function ($data) {
                        return Html::img(Yii::$app->params['server'] . $data['picture'], ['width' => '120px','height' => '90px']);
                    },
                        ],

                'first_name',
                'last_name',
                ['class' => 'yii\grid\CheckboxColumn'],
            ],
        ]);
        ?>
    </div>


    <?php ActiveForm::end(); ?>
    <div class="box-footer">
        <div class="row pull-right">
            <input type="submit" class="btn btn-success" id="btn-activate" value="Activate" />
        </div>

    </div>
</div>
