<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\api\modules\v1\models\User */
/* @var $form yii\widgets\ActiveForm */

$model = new app\api\modules\v1\models\User();

if (Yii::$app->session->hasFlash('success')):
    ?>
    <div class="alert alert-success alert-dismissable">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button" style="color:white;font-weight: bolder">×</button>
        <h4><i class="icon fa fa-check"></i>Password Reset!</h4>
    <?= Yii::$app->session->getFlash('success') ?>
    </div>
<?php endif; ?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Reset Password</h3>
    </div>
<?php $form = ActiveForm::begin(['action' => ['user/resetpw'], 'method' => 'post',]); ?>

    <div class="box-body">
        <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

        <div class="box-footer">
            <div class="row pull-right">
                <input type="submit" class="btn btn-success"  value="Submit" />
            </div>

        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>


