<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\api\modules\v1\models\SearchUser */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                    'attribute' => 'picture',
                    'format' => 'html',
                    'value' => function ($data) {
                        return Html::img(Yii::$app->params['server'] . $data['picture'], ['width' => '120px','height' => '90px']);
                    },
                        ],
                        
                        'first_name',
                        'last_name',
                        'email:email',
                           // 'username',
                        'phone',
                        //   'picture',
                        //'gender',
                        'designation',
                        'company_name',
                        //'qualification',
                        'active',
                        // 'lastmodified',
                        ['class' => 'yii\grid\ActionColumn'],
                    ],
    ]); ?>
</div>
