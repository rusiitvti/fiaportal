<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\api\modules\v1\models\Venue */
/* @var $form yii\widgets\ActiveForm */
$this->registerJsFile(
    'https://maps.googleapis.com/maps/api/js?key=AIzaSyDikL3ig89d-87OoFCY7svrSp1Jwt11Wck&libraries=places'
);


$script = <<< JS

            window.onload = function () {
                initialize();
            };
JS;
$this->registerJs($script);  
?>

<div class="venue-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'location')->dropDownList(
         yii\helpers\ArrayHelper::map($model->Locations, 'value', 'value'),
              ['prompt'=>'Select']
            ) ?>
    <?= $form->field($model, 'address')->textInput(['maxlength' => true, 'id'=> 'add']) ?>
    <div class="col-md-12 col-sm-12 col-xs-12" id="gmap" style="height: 300px">
    </div>
    <?= $form->field($model, 'latitude')->textInput(['maxlength' => true, 'id'=>'lat']) ?>
    <?= $form->field($model, 'longtitude')->textInput(['maxlength' => true, 'id'=>'lon']) ?>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php

$script1 = <<< JS
            var map;
            var geocoder;
        
//            window.onload = function () {
//                initialize();
//            };
        
            //google.maps.event.addDomListener(window, 'load', initialize);
        
            function initialize() {
                var myLatlng = new google.maps.LatLng(-18.13437, 178.44104);
                var myOptions = {
                    zoom: 16,
                    center: myLatlng,
                    mapTypeId: google.maps.MapTypeId.HYBRID
                }
                map = new google.maps.Map(document.getElementById("gmap"), myOptions);
                // marker refers to a global variable
                marker = new google.maps.Marker({
                    position: myLatlng,
                    map: map
                });

                google.maps.event.addListener(map, "click", function (event) {
                    // get lat/lon of click
                    var clickLat = event.latLng.lat();
                    var clickLon = event.latLng.lng();

                    // show in input box
                    document.getElementById("lat").value = clickLat.toFixed(5);
                    document.getElementById("lon").value = clickLon.toFixed(5);

//                     var marker = new google.maps.Marker({
//                     position: new google.maps.LatLng(clickLat, clickLon),
//                     map: map
//                     });
                });
        
                
            }

            
            function FindLocation() {
               
                geocoder = new google.maps.Geocoder();
                initialize();

                var address = document.getElementById("add").value;
                geocoder.geocode({'address': address}, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        map.setCenter(results[0].geometry.location);
                        var marker = new google.maps.Marker({
                            map: map,
                            position: results[0].geometry.location
                        });

                    }
                    else {
                        alert("Google could not find that address please manually pin it " + status);
                    }
                });

            }
        
            $("#add").change(FindLocation);
JS;
$this->registerJs($script1);