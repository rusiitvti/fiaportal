<?php


use richardfan\sortable\SortableGridView;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<?= SortableGridView::widget([
            'dataProvider' => $sess,

            // SortableGridView Configurations
            'sortUrl' => \yii\helpers\Url::to(['session/sortItem']),
            'sortingPromptText' => 'Loading...',
            'failText' => 'Fail to sort',

            'columns' => [
                // Data Columns
                [
                    'attribute' => 'events_id',
                    'value' => 'events.name'
                ],
                'program',
                [
                    'attribute' => 'speaker_id',
                    'value' => function ($model) {
                        //array_push($model->totalDays, totalDays, $model->class->date);
                        return $model->speakers;
                    },
                ],
                [
                    'attribute' => 'moderator_id',
                    'value' => function ($model) {
                        //array_push($model->totalDays, totalDays, $model->class->date);
                        return $model->moderator;
                    },
                ],
                'session_date',
                'start_time',
                'end_time',
                //'active',
                //'lastmodified',
                // 'events_id',
                // 'venue_id',
                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); 
?>

