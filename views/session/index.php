<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\api\modules\v1\models\Events;
use richardfan\sortable\SortableGridView;
use yii\data\ActiveDataProvider;
use app\api\modules\v1\models\Session;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sessions';
$this->params['breadcrumbs'][] = $this->title;

$session = new app\api\modules\v1\models\Session();

if (!empty($_GET)){
    $eventid = $_GET['eventid'];
    $model = Events::findOne($eventid);
}

?>
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        <p>
            <?= Html::a('Create Session', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    </div>
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body">

       <?=
        $form->field($session, 'event')->dropDownList(
                ArrayHelper::map(Events::find()->all(), 'id', 'name'), ['prompt' => 'Select Events']
        )
        ?>
        
     
    </div>
    <?php ActiveForm::end(); ?>

    <?php
        if (!empty($_GET)){
         
        $sess= Session::find()->where(['active'=>1])->andWhere(['events_id'=>$eventid])->orderBy([
                'events_id' => SORT_ASC,
                'order' => SORT_ASC]);
        
        $dataProvider = new ActiveDataProvider([
            'query' => $sess,
        ]);?>
   
    <div style="font-size: 20px; margin-left: 10px; margin-bottom:30px">
        <?= Html::getAttributeValue($model, 'name') ?>
    </div>
    <?= SortableGridView::widget([
            'dataProvider' => $dataProvider,

            // SortableGridView Configurations
            'sortUrl' => \yii\helpers\Url::to(['sortItem']),
            'sortingPromptText' => 'Loading...',
            'failText' => 'Fail to sort',

            'columns' => [
                // Data Columns
               
                'program',
                [
                    'attribute' => 'speaker_id',
                    'value' => function ($model) {
                        //array_push($model->totalDays, totalDays, $model->class->date);
                        return $model->speakers;
                    },
                ],
                [
                    'attribute' => 'moderator_id',
                    'value' => function ($model) {
                        //array_push($model->totalDays, totalDays, $model->class->date);
                        return $model->moderator;
                    },
                ],
                'session_date',
                'start_time',
                'end_time',
                //'active',
                //'lastmodified',
                // 'events_id',
                // 'venue_id',
                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); 
?>
    
    <?php    
        } ?>
   
</div>
