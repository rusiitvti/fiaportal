<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\api\modules\v1\models\Events;
use app\api\modules\v1\models\Venue;
use dosamigos\datepicker\DatePicker;
use kartik\time\TimePicker;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\api\modules\v1\models\Session */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Create Session</h3>
    </div>

    <?php $form = ActiveForm::begin(); ?>

    <div class="box-body">
        <?=
        $form->field($model, 'events_id')->dropDownList(
                ArrayHelper::map(Events::find()->all(), 'id', 'name'), ['prompt' => 'Select Events']
        )
        ?>
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12">
                <?=
                $form->field($model, 'start_time')->widget(TimePicker::classname(), [
                    'options' => [
                        'readonly' => false,
                    ]
                ]);
                ?>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12">
                <?=
                $form->field($model, 'end_time')->widget(TimePicker::classname(), [
                    'options' => [
                        'readonly' => false,
                    ]
                ]);
                ?>
            </div>

        </div>
     

        <?=
        $form->field($model, 'session_date')->widget(
                DatePicker::className(), [
            // inline too, not bad
            'inline' => false,
            // modify template for custom rendering
            //'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
            'clientOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-m-dd'
            ]
        ]);
        ?>
        <?= $form->field($model, 'program')->textInput(['maxlength' => true]) ?>
        
        <?php
            if (!$model->isNewRecord) {
                $usertype = $model->sessiondelegators;
                $selecteds = [];
                foreach ($usertype as $utype) {
                    if($utype->type == 'speaker'){
                        $id = $utype->user_id;
                        $selecteds[] = $id;
                    }
                }         

                $model->speaker_id = $selecteds;
               // echo 'update';

             echo  $form->field($model, 'speaker_id')->widget(Select2::classname(), [
                    'data' => $model->SpeakerDropdown,
                    'options' => ['multiple' => true],
                ]);

            } else {
                // echo 'new record';
    //          
                echo  $form->field($model, 'speaker_id')->widget(Select2::classname(), [
                    'data' => $model->SpeakerDropdown,
                    'language' => 'de',
                    'options' => ['multiple' => true, 'placeholder' => 'Select Speaker'],
                    'pluginOptions' => [
                    'allowClear' => true
                    ],
                ]);
            }
        ?>
        
        <?php
            if (!$model->isNewRecord) {
                $usertype = $model->sessiondelegators;
                $selecteds = [];
                foreach ($usertype as $utype) {
                    if($utype->type == 'moderator'){
                        $id = $utype->user_id;
                        $selecteds[] = $id;
                    }
                }         

                $model->moderator_id = $selecteds;
               // echo 'update';

             echo  $form->field($model, 'moderator_id')->widget(Select2::classname(), [
                    'data' => $model->ModeratorDropdown,
                    'options' => ['multiple' => true],
                ]);

            } else {
                // echo 'new record';
    //          
                echo  $form->field($model, 'moderator_id')->widget(Select2::classname(), [
                    'data' => $model->ModeratorDropdown,
                    'language' => 'de',
                    'options' => ['multiple' => true, 'placeholder' => 'Select Moderator'],
                    'pluginOptions' => [
                    'allowClear' => true
                    ],
                ]);
            }
        ?>
        
        <?php
            if (!$model->isNewRecord) {
                $usertype = $model->sessiondelegators;
                $selecteds = [];
                foreach ($usertype as $utype) {
                    if($utype->type == 'guestspeaker'){
                        $id = $utype->user_id;
                        $selecteds[] = $id;
                    }
                }         

                $model->gspeaker_id = $selecteds;
               // echo 'update';

             echo  $form->field($model, 'gspeaker_id')->widget(Select2::classname(), [
                    'data' => $model->GspeakerDropdown,
                    'options' => ['multiple' => true],
                ]);

            } else {
                // echo 'new record';
    //          
                echo  $form->field($model, 'gspeaker_id')->widget(Select2::classname(), [
                    'data' => $model->GspeakerDropdown,
                    'language' => 'de',
                    'options' => ['multiple' => true, 'placeholder' => 'Select Guest Speaker'],
                    'pluginOptions' => [
                    'allowClear' => true
                    ],
                ]);
            }
        ?>
        
        
        <?=
        $form->field($model, 'hasdiscussion')->dropDownList(
                array(1 => 'Yes', 0 => 'No'), ['prompt' => 'Select Discussion']
        )
        ?>
        
        <?=
        $form->field($model, 'isdebate')->dropDownList(
                array(1 => 'Yes', 0 => 'No'), ['prompt' => 'Select Polling']
        )
        ?>
        
        <?= $form->field($model, 'order')->dropDownList(range(1, 50)) ?>

        <?=
        $form->field($model, 'venue_id')->dropDownList(
                ArrayHelper::map(Venue::find()->all(), 'id', 'name'), ['prompt' => 'Select Venue']
        )
        ?>

        <div class="box-footer">
    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>

</div>
