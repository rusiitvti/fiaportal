<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\api\modules\v1\models\Sponsors;
use app\api\modules\v1\models\SponsorType;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\api\modules\v1\models\Sponsors */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Create Sponsors</h3>
    </div>


<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="box-body">

        <?= $form->field($model, 'companyname')->textInput(['maxlength' => true]) ?>

        <?php if($model->isNewRecord){
            echo $form->field($model, 'logo')->fileInput();
        }else{
             echo Html::img(Yii::$app->params['server'] . $model->logo, ['width' => '150px']);
             echo $form->field($model, 'logo')->fileInput();
        }
        ?>

        <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

        <?=
        $form->field($model, 'sponsortype_id')->dropDownList(
                ArrayHelper::map(SponsorType::find()->all(), 'id', 'type'), ['prompt' => 'Select Sponsor']
        )
        ?>
    </div>

    <div class="box-footer">
    <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

<?php ActiveForm::end(); ?>

</div>
