<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\api\modules\v1\models\SearchSponsors */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sponsors';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        <?php Pjax::begin(); ?>
        <p>
            <?= Html::a('Create Sponsor', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    </div>
    <div class="box-body">
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
               
                // 'id',
                [
                    'attribute' => 'logo',
                    'format' => 'html',
                    'value' => function ($data) {
                        return Html::img(Yii::$app->params['server'] . $data['logo'], ['width' => '200px','height'=>'70px']);
                    },
                 ],
                'companyname',
                
                        // 'lastmodified',
                        //'sponsortype_id',
                        //'active',
                        //'name',
                  'url:url',
                  [
                            'attribute' => 'sponsortype_id',
                            'value' => function ($model) {
                                //array_push($model->totalDays, totalDays, $model->class->date);
                                return $model->types;
                            },
                  ],
                        ['class' => 'yii\grid\ActionColumn'],
                    ],
                ]);
                ?>
                <?php Pjax::end(); ?>
    </div>
</div>
