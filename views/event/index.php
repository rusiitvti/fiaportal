<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Events';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        <p>
            <?= Html::a('Create Events', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    </div>
    <div class="box-body">


        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
              
//            'id',
                [
                    'attribute' => 'image_cover',
                    'format' => 'html',
                    'value' => function ($data) {
                        return Html::img(Yii::$app->params['server'] . $data['image_cover'], ['width' => '200px', 'height'=>'90px']);
                    },
                        ],
                'name',
                'description',
                //'image_cover',
                
                        'start_date',
                        'end_date',
                        [
                            'attribute' => 'venue_id',
                            'value' => 'venue.name'
                        ],
                        //  'active',
                        // 'lastmodified',
                        // 'venue_id',
                        ['class' => 'yii\grid\ActionColumn'],
                    ],
                ]);
                ?>
    </div>
</div>
