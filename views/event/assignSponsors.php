<?php

//use yii\helpers\Html;
use yii\widgets\ActiveForm;
//use dosamigos\datepicker\DatePicker;
//use app\api\modules\v1\models\Venue;
use yii\helpers\ArrayHelper;
use app\api\modules\v1\models\Events;
use app\api\modules\v1\models\EventsSponsors;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\api\modules\v1\models\Events */
/* @var $form yii\widgets\ActiveForm */
$eventsponsors = new app\api\modules\v1\models\EventsSponsors();

$this->title = 'Assign Sponsors to Event';
?>

<div class="box box-primary">
   <div class="box-header with-border">
       <h3 class="box-title">Assign Sponsors to an Event</h3>
   </div>
   <?php $form = ActiveForm::begin(['action' => ['event/assignevent'], 'method' => 'post',]); ?>

   <div class="box-body">

       <?=
       $form->field($eventsponsors, 'events_id')->dropDownList(
               ArrayHelper::map(Events::find()->all(), 'id', 'name'), ['prompt' => 'Select Events']
       )
       ?>

       <br>
       <h4>List of Sponsors</h4>
       <br>
      
   </div>


<?php ActiveForm::end(); ?>

   <div id="ajaxdataloaded">

    </div>
    
</div>