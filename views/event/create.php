<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\api\modules\v1\models\Events */

$this->title = '';
$this->params['breadcrumbs'][] = ['label' => 'Events', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="events-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
