<?php

use yii\grid\GridView;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="table">
    <?=
    GridView::widget([
        'dataProvider' => $eventattendees,
        'filterModel' => $searchModel,
        'id' => 'usergrid',
        'columns' => [

            'first_name',
            'last_name',
            ['class' => 'yii\grid\CheckboxColumn'],
        ],
    ]);
    ?>
    <div class="box-footer">
        <div class="row pull-right">
            <input type="submit" class="btn btn-success" id="btn-assignUser" value="Assign" />
            <!--input type="submit" class="btn btn-danger" id="btn-removeuser" value="Remove" /-->
        </div>

    </div>
</div>
