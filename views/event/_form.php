<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;
use app\api\modules\v1\models\Venue;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\api\modules\v1\models\Events */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Create Events</h3>
    </div>

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="box-body">
        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

        <?php if($model->isNewRecord){
            echo $form->field($model, 'file')->fileInput();
        }else{
             echo Html::img(Yii::$app->params['server'] . $model->image_cover, ['width' => '150px']);
             echo $form->field($model, 'file')->fileInput();
        }
        ?>

        <?=
        $form->field($model, 'start_date')->widget(
                DatePicker::className(), [
            // inline too, not bad
            'inline' => false,
            // modify template for custom rendering
            //'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
            'clientOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-m-dd'
            ]
        ]);
        ?>

        <?=
        $form->field($model, 'end_date')->widget(
                DatePicker::className(), [
            // inline too, not bad
            'inline' => false,
            // modify template for custom rendering
            //'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
            'clientOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-m-dd'
            ]
        ]);
        ?>

<?=
$form->field($model, 'venue_id')->dropDownList(
        ArrayHelper::map(Venue::find()->all(), 'id', 'name'), ['prompt' => 'Select Venue']
)
?>

    </div>
    <div class="box-footer">
<?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

<?php ActiveForm::end(); ?>

</div>
