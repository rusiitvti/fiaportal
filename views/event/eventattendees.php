<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\api\modules\v1\models\EventsAtendees;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use app\api\modules\v1\models\Events;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Event Attendees';
$this->params['breadcrumbs'][] = $this->title;

$event = new app\api\modules\v1\models\Events();

if (!empty($_GET)){
    $eventid = $_GET['eventid'];
    $model = Events::findOne($eventid);
}

?>
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        
    </div>
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body">

       <?=
        $form->field($event, 'event')->dropDownList(
                ArrayHelper::map(Events::find()->all(), 'id', 'name'), ['prompt' => 'Select Events']
        )
        ?>
        
     
    </div>
    <?php ActiveForm::end(); ?>

    <?php
        if (!empty($_GET)){
         
        $user= EventsAtendees::find()->where(['active'=>1])->andWhere(['events_id'=>$eventid]);
        
        $dataProvider = new ActiveDataProvider([
            'query' => $user,
        ]);?>
   
    <div style="font-size: 20px; margin-left: 10px; margin-bottom:30px; text-transform: uppercase">
        <label>List of Attendees for <?= Html::getAttributeValue($model, 'name') ?></label>
    </div>
    <?= GridView::widget([
            'dataProvider' => $dataProvider,

            'columns' => [
                // Data Columns
               
            [
                'label'=> 'First Name',
                'attribute' => 'user_id',
                'value' => 'user.first_name'
            ],
            [
                'label'=> 'Last Name',
                'attribute' => 'user_id',
                'value' => 'user.last_name'
            ],
                
                //['class' => 'yii\grid\ActionColumn'],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Actions',
                'headerOptions' => ['style' => 'color:#337ab7'],
                'template' => '{delete}',
                'buttons' => [

                  'delete' => function ($url, $model) {
                      return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                  'title' => Yii::t('app', 'lead-delete'),
                                  'data' => [
                'confirm' => 'Are you sure you want to delete this attendee?',
                'method' => 'post',
            ],
                      ]);
                  }

                ],
                'urlCreator' => function ($action, $model, $key, $index) {

                  if ($action === 'delete') {
                      $url =\yii\helpers\Url::to(['/event/inactiveuser', 'id' => $model->user_id,'eventid'=>$model->events_id]);
                      return $url;
                  }

                }
            ],
            ],
        ]); 
?>
    
    <?php    
        } ?>
   
</div>
