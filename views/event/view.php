<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\api\modules\v1\models\Events */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Events', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="events-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id, 'venue_id' => $model->venue_id], ['class' => 'btn btn-primary']) ?>
        <?=
        Html::a('Delete', ['delete', 'id' => $model->id, 'venue_id' => $model->venue_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ])
        ?>
    </p>
  
    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [

            'name',
            'description',
            'start_date',
            'end_date',
           // 'active',
            //'lastmodified',
            [
                'attribute' => 'image_cover',
                'value' => Yii::$app->params['server'].$model->image_cover,
                'format' => ['image', ['width' => '100', 'height' => '100']],
            ],
             [
                'attribute' => 'sponsorList',
                'value' => '<div class="well">' . $model->sponsors . '</div',
                'format' => ['html'],
            ],
           
//                 'sponsorList' => $model->sponsors,
                
             //   'format' => ['image', ['width' => '100', 'height' => '100']],
            
            
        ],
    ])
    ?>

</div>
