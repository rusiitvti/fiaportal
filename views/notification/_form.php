<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\api\modules\v1\models\Events;

/* @var $this yii\web\View */
/* @var $model app\api\modules\v1\models\Notification */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Send Notification</h3>
    </div>

    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body">
         <?=
        $form->field($model, 'eventid')->dropDownList(
                ArrayHelper::map(Events::find()->all(), 'id', 'name'), ['prompt' => 'Select Event']
        )
        ?>
        <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'body')->textarea(['maxlength' => true]) ?>

        <div class="form-group">
            <?= Html::submitButton('SEND', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
