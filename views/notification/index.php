<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Notifications';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notification-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Notification', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'title',
            'body',
            ['class' => 'yii\grid\ActionColumn',
             'visibleButtons' => [
                'update' => function ($model) {
                    return \Yii::$app->user->can('updatePost', ['post' => $model]);
                },
                'view' => function ($model) {
                    return \Yii::$app->user->can('updatePost', ['post' => $model]);
                },
            ]
                
                ],
        ],
    ]); ?>
</div>
