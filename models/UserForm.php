<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\helpers\Security;
use yii\web\IdentityInterface;

class UserForm extends ActiveRecord implements IdentityInterface {

    public $id;
    public $username;
    public $password;
    public $userid;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'user';
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username) {
        //return static::findOne(['username' => $username]);
        $user = self::find()
                ->where([
                    "username" => $username
                ])
                ->one();
        if (!count($user)) {
            return null;
        } else {
            $password = $user->getAttribute('password');
            $username = $user->getAttribute('username');
            $id = $user->getAttribute('id');
            // exit;
            $user->password = $password;
            $user->username = $username;
            $user->id = $id;
            //  echo $user->password;
            // exit;
            //  $this->password = $user->password;
        }
        return new static($user);
    }

    /**
     * @inheritdoc
     */
    public function getId() {
        return $this->id;
    }

    public static function setPassword($password) {
        $this->password = $password;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password) {
//        echo $this->password;
//        exit;
        return Yii::$app->getSecurity()->validatePassword($password, $this->password);
        //   return $this->password === $password;
    }

    public function getAuthKey() {
        
    }

    public function validateAuthKey($authKey) {
        
    }

    public static function findIdentity($id) {
        
    }

    public static function findIdentityByAccessToken($token, $type = null) {
        
    }

}
