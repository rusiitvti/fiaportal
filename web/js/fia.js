/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {

     //$('#btn-assignUser').on('click', function () {
    //alert('why');
    $('#btn-assignUser').on('click', function () {
        //var eventsid = $('#events-event_id').val();

        var eventsid = getParams('eventid');//get event id from url -> function name getParams


        //alert(eventsid);
        //$('#usergrid').yiiGridView('getSelectedRows');
        var userids = $('#userlistgrid').yiiGridView('getSelectedRows');
        //alert(eventsid);
        $.ajax({
            url: 'assignevent',
            type: 'post',
            dataType: 'json',
            data: {
                eventid: eventsid,
                users: userids,
                _csrf: '<?=Yii::$app->request->getCsrfToken()?>'
            },
            success: function (data) {
                //alert(data);
                location.reload();
            },
            error: function (errormessage) {

                //do something else
                location.reload();
                alert("User added to this event");

            }
        });
    });

    function getRows()
    {
        var strvalue = [];
        $('input[name="selection[]"]:checked').each(function () {
            if (strvalue !== "")
                strvalue.push(this.value);
           
        });
        return strvalue;
    }


    function getParams(sParam)
    {
        var sPageURL = window.location.search.substring(1);
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++)
        {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == sParam)
            {
                return sParameterName[1];
            }

        }
    }


    $('#btn-removeuser').on('click', function () {
        var eventsid = $('#eventsatendees-events_id').val();
        var userids = $('#usergrid').yiiGridView('getSelectedRows');
        $.ajax({
            url: 'deactivateuser',
            type: 'post',
            dataType: 'json',
            data: {
                eventid: eventsid,
                users: userids,
                _csrf: '<?=Yii::$app->request->getCsrfToken()?>'
            },
            success: function (data) {
                alert(data);
            },
            error: function (errormessage) {

                //do something else
                alert("User removed from this event");

            }
        });
    });

    $('#btn-assignSponsors').on('click', function () {
        var eventsid = $('#eventssponsors-events_id').val();
        var sponsorsid = $('#sponsorgrid').yiiGridView('getSelectedRows');
        $.ajax({
            url: 'transfersponsor',
            type: 'post',
            dataType: 'json',
            data: {
                eventid: eventsid,
                sponsors: sponsorsid,
                _csrf: '<?=Yii::$app->request->getCsrfToken()?>'
            },
            success: function (data) {
                alert(data);
            },
            error: function (errormessage) {

                //do something else
                alert("Sponsor added to this event");

            }
        });
    });

    $('#btn-activate').on('click', function () {
        var userids = $('#usergrid').yiiGridView('getSelectedRows');
        $.ajax({
            url: 'activate',
            type: 'post',
            dataType: 'json',
            data: {
                users: userids,
                _csrf: '<?=Yii::$app->request->getCsrfToken()?>'
            },
            success: function (data) {
                alert(data);
            },
            error: function (errormessage) {

                //do something else
                alert("User activated");

            }
        });
    });


    $('#eventsatendees-events_id').on('change', function () {
        var eventid = $('#eventsatendees-events_id').val();
        $.ajax({
            url: 'assign',
            type: 'post',
            dataType: 'json',
            data: {
                eventid: eventid,
                _csrf: '<?=Yii::$app->request->getCsrfToken()?>'
            },
            success: function (data) {
//                var loadtemp = $.parseJSON(data);
//                alert(data);
                $('#ajaxloaded').html(data);
            },
            error: function (errormessage) {
                // alert(errormessage);
                //do something else
                //alert("Error getting users of this event");

            }
        });


    });

    $('#eventssponsors-events_id').on('change', function () { 
        var eventid = $('#eventssponsors-events_id').val();//this is getting the event id
        $.ajax({
            url: 'getnotsponsors',//this is the function that is posting to
            type: 'post',
            dataType: 'json',
            data: {
                eventid: eventid,//that is the same event id from the top
                
                _csrf: '<?=Yii::$app->request->getCsrfToken()?>'
            },
            success: function (data) {
                  //now when the function success, reload the data into a __partialview
                $('#ajaxdataloaded').html(data);//ajax loaded 
            },
            error: function (errormessage) {
                // alert(errormessage);
                //do something else
                //alert("Error getting users of this event");

            }
        });
        

    });


     $('#eventsindex-event_id').on('change', function () { 
        var eventid = $('#eventsindex-event_id').val();//this is getting the event id
        $.ajax({
            url: 'a',//this is the function that is posting to
            type: 'post',
            dataType: 'json',
            data: {
                eventid: eventid,//that is the same event id from the top
                
                _csrf: '<?=Yii::$app->request->getCsrfToken()?>'
            }
        });
        

    });
    
    $('#polls-question').on('change', function () {
        var qtnid = $('#polls-question').val();
        $.ajax({
            url: 'getcount',
            type: 'post',
            dataType: 'json',
            data: {
                qtnid: qtnid
                //_csrf: '<?=Yii::$app->request->getCsrfToken()?>'
            },
            success: function (data) {
                //var loadtemp = $.parseJSON(data);
                //alert(loadtemp);
                //$('#ajaxloaded').html(data);
            },
            error: function (errormessage) {
                 //alert(errormessage);
                //do something else
                //alert("Error ");

            }
        });

    });
    
    $('#session-event').on('change', function () {
        var eventid = $('#session-event').val();
        $.ajax({
            url: 'index',
            type: 'post',
            dataType: 'json',
            data: {
                eventid: eventid,
                _csrf: '<?=Yii::$app->request->getCsrfToken()?>'
            },
            success: function (data) {
//                var loadtemp = $.parseJSON(data);
                ///alert(data);
                $('#loadsessions').html(data);
            },
            error: function (errormessage) {
               //alert(errormessage);
                //do something else
                //alert("Error getting users of this event");

            }
        });

    });
    
    $('#events-event').on('change', function () {
        var event_id = $('#events-event').val();
        $.ajax({
            url: 'eventattendees',
            type: 'post',
            dataType: 'json',
            data: {
                event_id: event_id,
                _csrf: '<?=Yii::$app->request->getCsrfToken()?>'
            }
        });

    });

});

$('#events-event_id').on('change', function () {
        var event_id = $('#events-event_id').val();
        $.ajax({
            url: 'assign',
            type: 'post',
            dataType: 'json',
            data: {
                event_id: event_id,
                _csrf: '<?=Yii::$app->request->getCsrfToken()?>'
            }
        });

    });



// function GetURLParameter(sParam)

// {
//     var sPageURL = window.location.search.substring(1);

//     var sURLVariables = sPageURL.split('&');

//     for (var i = 0; i < sURLVariables.length; i++)

//     {
//         var sParameterName = sURLVariables[i].split('=');

//         if (sParameterName[0] == sParam)
//         {
//             return sParameterName[1];
//         }
//     }
// }​




